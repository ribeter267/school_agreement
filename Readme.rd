1、网站，确认， 有修改提交备注
2、学生信息绑定  任何时候都可以看到绑定按钮，绑定后，简历显示认证
3、绑定后，已填写的基础信息被覆盖
4、已存在就业方案信息学生不能填写， 不存在就业方案信息学生可以填写



MK@#
学院， 学院号为字符串， 学院号不再做为表的主键， 而是有单独的学院号栏
专业， 专业号同学院号， 存单独的专业号栏目， 专业表中，同时保存学院信息， 以便清空和加入

MK@#
求职招聘 中的院校，学院，专业信息 修改成录入， 为对应的名称

MK@#
学校老师的  院校，学院，专业 处理( 添加对应的名称 ) 区内学生保留选择信息
school		school_name
college		college_name
major		major_name		


truncate table tx_schoolagreement_domain_model_gender;
truncate table tx_schoolagreement_domain_model_pyfs;
truncate table tx_schoolagreement_domain_model_schoolsystem;
truncate table tx_schoolagreement_domain_model_sfslb;
truncate table tx_schoolagreement_domain_model_knslb;
truncate table tx_schoolagreement_domain_model_byqx;
truncate table tx_schooljobs_domain_model_jobcategory;
truncate table tx_schoolagreement_domain_model_bdzqflb;
truncate table tx_schoolagreement_domain_model_sfmyjx;
truncate table tx_schooljobs_domain_model_major;
truncate table tx_schooljobs_domain_model_national;
truncate table tx_schooljobs_domain_model_political;
truncate table tx_schooljobs_domain_model_diploma;
truncate table tx_schooljobs_domain_model_area;
truncate table tx_schooljobs_domain_model_industry;
truncate table tx_schooljobs_domain_model_companyproperty;



字典参看 毕业生信息数据结构(V1.7)--内蒙.xlsx


性别数据表				tx_schoolagreement_domain_model_gender
培养方式数据表			tx_schoolagreement_domain_model_pyfs
学制数据表				tx_schoolagreement_domain_model_schoolsystem
师范生类别数据表		tx_schoolagreement_domain_model_sfslb
困难生类别数据表		tx_schoolagreement_domain_model_knslb
毕业去向数据表			tx_schoolagreement_domain_model_byqx
工作职位类别数据表		tx_schooljobs_domain_model_jobcategory
报到证签发类别数据表	tx_schoolagreement_domain_model_bdzqflb
授课语言数据表			tx_schoolagreement_domain_model_sfmyjx
院校数据表       		tx_schooljobs_domain_model_school 
学院数据表       		tx_schooljobs_domain_model_college
专业数据表       		tx_schooljobs_domain_model_major
民族数据表       		tx_schooljobs_domain_model_national
政治面貌数据表   		tx_schooljobs_domain_model_political
学历数据表      		tx_schooljobs_domain_model_diploma
地区数据表       		tx_schooljobs_domain_model_area
行业数据表        		tx_schooljobs_domain_model_industry 
公司性质数据表   		tx_schooljobs_domain_model_companyproperty
