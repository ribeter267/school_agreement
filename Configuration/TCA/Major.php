<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_schoolagreement_domain_model_major'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_schoolagreement_domain_model_major']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, xxdm, xxmc, xslx, zyh, yzsh, yxsmc, zymc, zyjc',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, xxdm, xxmc, xslx, zyh, yzsh, yxsmc, zymc, zyjc, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_schoolagreement_domain_model_major',
				'foreign_table_where' => 'AND tx_schoolagreement_domain_model_major.pid=###CURRENT_PID### AND tx_schoolagreement_domain_model_major.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'xxdm' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_major.xxdm',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'xxmc' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_major.xxmc',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'xslx' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_major.xslx',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.xslx.I.0', 0),
					array('LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.xslx.I.1', 1),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'zyh' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_major.zyh',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'yzsh' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_major.yzsh',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'yxsmc' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_major.yxsmc',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'zymc' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_major.zymc',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'zyjc' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_major.zyjc',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		
	),
);
