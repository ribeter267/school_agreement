<?php

class Tx_SchoolAgreement_Task_DictionaryImportTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask {
	
	/**
	 * soap 客户端
	 */
	protected $soap;
	
	/**
	 * execute
	 */
    public function execute() {
		
		$common = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TaoJiang\SchoolAgreement\Controller\CommonController');
		$this->soap = $common->soapClient();
		
		//院校数据表       		tx_schooljobs_domain_model_school 
		//学院数据表       		tx_schooljobs_domain_model_college
		$dictConfig = array(
			array(
				'dicttable' => 'tx_schoolagreement_domain_model_gender',
				'dictcode' => 'SEX',
			),
			array(
				'dicttable' => 'tx_schoolagreement_domain_model_pyfs',
				'dictcode' => 'BRINGUP',
			),
			array(
				'dicttable' => 'tx_schoolagreement_domain_model_schoolsystem',
				'dictcode' => 'LENCH',
			),
			array(
				'dicttable' => 'tx_schoolagreement_domain_model_sfslb',
				'dictcode' => 'SPEAKSPEC',
			),
			array(
				'dicttable' => 'tx_schoolagreement_domain_model_knslb',
				'dictcode' => 'PENURY',
			),
			array(
				'dicttable' => 'tx_schoolagreement_domain_model_byqx',
				'dictcode' => 'BYQXDM',
			),
			array(
				'dicttable' => 'tx_schooljobs_domain_model_jobcategory',
				'dictcode' => 'GZZWLBM',
			),
			array(
				'dicttable' => 'tx_schoolagreement_domain_model_bdzqflb',
				'dictcode' => 'BDZQFLBM',
			),
			array(
				'dicttable' => 'tx_schoolagreement_domain_model_sfmyjx',
				'dictcode' => 'SPEAKLANGUAGE',
			),
			array(
				'dicttable' => 'tx_schooljobs_domain_model_major',
				'dictcode' => 'SPEC',
			),
			array(
				'dicttable' => 'tx_schooljobs_domain_model_national',
				'dictcode' => 'NATION',
			),
			array(
				'dicttable' => 'tx_schooljobs_domain_model_political',
				'dictcode' => 'POLITY',
			),
			array(
				'dicttable' => 'tx_schooljobs_domain_model_diploma',
				'dictcode' => 'CHAGE',
			),
			array(
				'dicttable' => 'tx_schooljobs_domain_model_area',
				'dictcode' => 'DISTRICT',
			),
			array(
				'dicttable' => 'tx_schooljobs_domain_model_industry',
				'dictcode' => 'UNITWAY',
			),
			array(
				'dicttable' => 'tx_schooljobs_domain_model_companyproperty',
				'dictcode' => 'UNITTYPE',
			),
		);
		
		return $this->dataImport($dictConfig);
    }
	
	/**
	 * 数据导入
	 * @param $array $config
	 */
	protected function dataImport($config){
		foreach($config as $conf){
			$data = $this->soap->getDictionary(array('arg0'=>$conf['dictcode'], 'arg1'=>10000, 'arg2'=>0));
			if(isset($data->return) && is_array($data->return)){
				foreach($data->return as $d){
					if(intval($d->dictvalue) != 0){
						$sql = 'replace into '.$conf['dicttable'].'(uid, title, deleted, hidden) values('.intval($d->dictvalue).', "'.$d->valueremark.'",0 ,0)';
						$res = $GLOBALS['TYPO3_DB']->sql_query($sql);
						$GLOBALS['TYPO3_DB']->sql_free_result($res);
					}
				}
			}else{
				var_dump($conf['dictcode'].' 返回数据不存在， 导入中止');
				return false;
			}
		}
		return true;
	}
}