<?php

class Tx_SchoolAgreement_Task_SchoolImportTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask {
	
	/**
	 * soap 客户端
	 */
	protected $soap;
	
	/**
	 * execute
	 */
    public function execute() {
		
		$common = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TaoJiang\SchoolAgreement\Controller\CommonController');
		$this->soap = $common->soapClient();
		
		//院校数据表       		tx_schoolagreement_domain_model_school 
		//学院数据表       		tx_schoolagreement_domain_model_college
		//专业数据表			tx_schoolagreement_domain_model_major
		
		return $this->schoolImport();
    }
	
	
	//getEduNameList(); 查询出所有学校基本信息。
	//getEduDepartmentList(String yxdm,String xslx); //查询出所有学校下的院系信息，yxdm:学校代码，xslx学生类型（0：本专科，1：研究生）。
	//getEduSpecList(String yxdm, String yzsh,String xslx,String lddm); //查询出其它一年度下的相应专业，yxdm:学校代码，yzsh:院、系、所号，xslx:学生类型同上，lddm:年度代码。
	
	/**
	 * 导入院校数据
	 */
	protected function schoolImport(){
		$data = $this->soap->getEduNameList();
		if(isset($data->return) && is_array($data->return)){
			foreach($data->return as $d){
				$sql = 'replace into tx_schoolagreement_domain_model_school (uid, xxmc, xxywmc, deleted, hidden) values('.intval($d->xxdm).', "'.$d->xxmc.'", "'.$d->xxywmc.'",0 ,0)';
				$res = $GLOBALS['TYPO3_DB']->sql_query($sql);
				$GLOBALS['TYPO3_DB']->sql_free_result($res);
				$this->collegeImport($d->xxdm);
			}
			return true;
		}
		return false;
	}
	
	
	/**
	 * 学院数据导入
	 */
	protected function collegeImport($xxdm,$xslx = 0){
		if($xslx > 1) return true;

		$data = $this->soap->getEduDepartmentList(array('arg0'=>$xxdm,'arg1'=>$xslx));
		if(isset($data->return)){
			if(is_array($data->return)){
				foreach($data->return as $d){
					$this->collegeDataSql($d);
			}}else{
				//ebug($data->return->xxdm,'$data->return->xxdm');
				$this->collegeDataSql($data->return);
			}
		}
		//debug($xslx,'$xslx');
		
		$xslx = $xslx + 1;
		$this->collegeImport($xxdm,$xslx);
	}
	
	
	/**
	 * 学院数据存储
	 * @param $d
	 * return void
	 */
	protected function collegeDataSql($d){
		if($d != null && $d->xxdm != '' && $d->yzsh != ''){
			if(!$GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid', 'tx_schoolagreement_domain_model_college','xxdm = "'.$d->xxdm.'" AND yzsh="'.$d->yzsh.'" AND deleted = 0 AND hidden = 0')){
				$GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_schoolagreement_domain_model_college',array(
					'xslx' => $d->xslx,
					'xxdm' => $d->xxdm,
					'xxmc' => $d->xxmc,
					'yxsjc' => $d->yxsjc,
					'yxsmc' => $d->yxsmc,
					'yxsywmc' => $d->yxsywmc,
					'yzsh' => $d->yzsh,
				));
			}
			$this->majorImport($xxdm, $d->yzsh, $xslx);
		}
	}
	
	
	/**
	 * 专业数据导入
	 */
	protected function majorImport($xxdm, $yzsh, $xslx){
		$data = $this->soap->getEduSpecList(array('arg0'=>$xxdm,'arg1'=>$yzsh,'arg2'=>$xslx,'arg3'=>date('Y')));
		if(isset($data->return)){
			if(is_array($data->return)){
				foreach($data->return as $d){
					$this->majorDataSql($d);
			}}else{
				$this->majorDataSql($data->return);
			}
			return true;
		}
		return false;
	}
	
	
	/**
	 * 专业数据存储
	 * @param $d
	 * return void
	 */
	protected function majorDataSql($d){
		if($d != null && $d->xxdm != '' && $d->yzsh != '' && $d->zyh != ''){
			if(!$GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid', 'tx_schoolagreement_domain_model_major','xxdm = "'.$d->xxdm.'" AND yzsh="'.$d->yzsh.'" AND zyh ="'.$d->zyh.'" AND deleted = 0 AND hidden = 0')){
				$GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_schoolagreement_domain_model_major',array(
					'xxdm' => $d->xxdm,
					'xxmc' => $d->xxmc,
					'xslx' => $d->xslx,
					'zyh' => $d->zyh,
					'yxsmc' => $d->yxsmc,
					'zymc' => $d->zymc,
					'zyjc' => $d->zyjc,
					'yzsh' => $d->yzsh,
				));
			}
		}
	}
	
}