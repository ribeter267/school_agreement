<?php

class Tx_SchoolAgreement_Task_StudentRefreshTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask {
	
	protected $soap;
	protected $jobRepository;
	protected $collegeRepository;
	protected $majorRepository;
	protected $genderRepository;
	protected $nationalRepository;
	protected $politicalRepository;
	protected $schoolRepository;
	protected $diplomaRepository;
	protected $pyfsRepository;
	protected $schoolsystemRepository;
	protected $sfslbRepository;
	protected $knslbRepository;
	protected $sfmyjxRepository;
	protected $companyPropertyRepository;
	protected $industryRepository;
	protected $areaRepository;
	protected $bdzqflbRepository;
	protected $jobCategoryRepository;
	protected $byqxRepository;
	
	/**
	 * execute
	 */
    public function execute() {
		
		$lddm = date('Y');
		$this->initializeObject();
		
		$common = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TaoJiang\SchoolAgreement\Controller\CommonController');
		$this->soap = $common->soapClient();
		
		
		$students = $this->jobRepository->findIssyncedStudent($lddm);
		if($students->count() > 0){
			foreach($students as $student){
				$this->getRemoteData($student);
				$this->jobRepository->update($student);
				$this->persistenceManager->persistAll();
			}
		}else{
			echo '无可用数据';
		}
		
		return true;
    }
	
	
	
	/**
	 * 获取远程数据, 并合并
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Student $student
	 * return void
	 */
	protected function getRemoteData(\TaoJiang\SchoolAgreement\Domain\Model\Job $student){
		
		
		//基础信息
		$info = $this->soap->getSources(array('arg0'=>$student->getIdCard()));
		if($info->return == null) return false;
		
		$student->setLddm($info->return->lddm); 											//lddm;//毕业年度
		$student->setXslx($info->return->xslx); 											//xslx;//学生类别：0：本专科，1：研究生
		
		$student->setSchool($this->schoolRepository->findByUid($info->return->yxdm));		//yxdm;//院校代码 //yxmc;//院校名称	
		$student->setCollege($this->collegeRepository->findByXydm($info->return->yxdm,$info->return->xydm)); 	//xydm;//学院代码 //xymc;//学院名称
		$student->setMajor($this->majorRepository->findByZydm($info->return->yxdm,$info->return->xydm,$info->return->zydm)); 		//zymc;//专业名称 //zydm;//专业代码	 //zy;//专业		
		$student->setClass($info->return->bjbh);											//bjbh;//班级
		$student->setSchoolName($info->return->yxmc);
		$student->setCollegeName($info->return->xymc);
		$student->setMajorName($info->return->zymc);
		
		
		$student->setKsh($info->return->ksh);												//ksh;//考生号		
		$student->setIdCard($info->return->sfzh);											//sfzh;//身份证号	
		$student->setName($info->return->xm);												//xm;//姓名
		$student->setGender($this->genderRepository->findByUid($info->return->xbdm));		//xbdm;//性别代码
		$student->setNational($this->nationalRepository->findByUid($info->return->mzdm));		//mzdm;//民族代码	
		$student->setPolitical($this->politicalRepository->findByUid($info->return->zzmmdm));//zzmmdm;//政治面貌代码	
		$student->setYxszsdm($info->return->yxszsdm);										//yxszsdm;//院校所在省代码	
		$student->setDiploma($this->diplomaRepository->findByUid($info->return->xldm));		//xldm;//学历代码	
		$student->setZyfx($info->return->zyfx);												//zyfx;//专业方向	
		$student->setPyfsdm($this->pyfsRepository->findByUid($info->return->pyfsdm));		//pyfsdm;//培养方式代码	
		$student->setNativetowns($this->areaRepository->findByUid($info->return->syszddm));	//syszddm;//生源所在地代码 //syszd;//生源所在地	
		$student->setSchoolsystem($this->schoolsystemRepository->findByUid($info->return->xz));	//xz;//学制		
		$student->setRxsj(new \DateTime($info->return->rxsj));								//rxsj;//入学时间	
		$student->setBysj(new \DateTime($info->return->bysj));								//bysj;//毕业时间	
		$student->setSfslbdm($this->sfslbRepository->findByUid($info->return->sfslbdm));	//sfslbdm;//师范生类别代码	
		$student->setKnslbdm($this->knslbRepository->findByUid($info->return->knslbdm));	//knslbdm;//困难生类别代码	
		$student->setDxhwpdw($info->return->dxhwpdw);										//dxhwpdw;//定向或委培单位	
		$student->setCampus($info->return->fxmc);											//fxmc;//分校名称	
		$student->setNumber($info->return->xh);												//xh;//学号
		$student->setBirthday(new \DateTime($info->return->csrq));							//csrq;//出生日期
		$student->setXysdm($info->return->xysdm);											//	xysdm;//协议书代码
		$student->setSfmyjx($this->sfmyjxRepository->findByUid($info->return->sfmyjx));		//	sfmyjx;//授课语言
		$student->setZsbh($info->return->zsbh);												//	zsbh;//证书编号
		//	bz;//备注
		//	csjyshdm;//初始化就业信息代码审核是否通过
		$student->setIsimp($info->return->isimp == 'true' ? true : $info->return->isimp);	//	isimp;//是否导入就业方案
		$student->setXysff($info->return->xysff == 'true' ? true : $info->return->xysff);	//	xysff;//协议书发放
		$student->setErrinfo($info->return->errinfo);										//	errinfo;//审核错误信息
		$student->setIssourcechecked($info->return->ischecked == 'true' ? true : $info->return->ischecked);	//	ischecked;//是否被确认
		$student->setIssynced(1); 															//基础信息已同步
		$student->setLocalGraduate(true);
		
		//扩展信息
		$infoPlus = $this->soap->getSourceExtend(array('arg0'=>$student->getIdCard()));
		if($infoPlus->return != null){
			$student->setCxsy($infoPlus->return->cxsy);										//cxsy;//城乡生源		
			$student->setRxqdaszdw($infoPlus->return->rxqdaszdw);							//rxqdaszdw;//入学前档案所在单位	
			$student->setDasfzrxx($infoPlus->return->dasfzrxx == 'true' ? true : $infoPlus->return->dasfzrxx);	//dasfzrxx;//档案是否转入学校	
			$student->setRxqhkszdpcs($infoPlus->return->rxqhkszdpcs);						//rxqhkszdpcs;//入学前户口所在地派出所	
			$student->setHksfzrxx($infoPlus->return->hksfzrxx == 'true' ? true : $infoPlus->return->hksfzrxx);		//hksfzrxx;//户口是否转入学校	
			$student->setMobile($infoPlus->return->sjhm);									//sjhm;//手机号码		
			$student->setEmail($infoPlus->return->dzxx);									//dzxx;//电子邮箱		
			$student->setQq($infoPlus->return->qqhm);										//qqhm;//QQ号码			
			$student->setHomeAddress($infoPlus->return->jtdz);								//jtdz;//家庭地址		
			$student->setHomeTelephone($infoPlus->return->jtdh);							//jtdh;//家庭电话		
			$student->setHomeZip($infoPlus->return->jtyb);									//jtyb;//家庭邮编	
		}
		
		//就业方案信息
		$infoJob = $this->soap->getJobs(array('arg0'=>$student->getIdCard()));
		if($infoJob->return != null){
		
			$student->setByqxdm($this->byqxRepository->findByUid($infoJob->return->byqxdm));  //byqxdm;//毕业去向代码		
			$student->setDwmc($infoJob->return->dwmc);											//dwmc;//单位名称		
			$student->setDwzzjgdm($infoJob->return->dwzzjgdm);									//dwzzjgdm;//单位组织机构代码	
			$student->setDwxzdm($this->companyPropertyRepository->findByUid($infoJob->return->dwxzdm));//dwxzdm;//单位性质代码		
			$student->getDwhydm($this->industryRepository->findByUid($infoJob->return->dwhydm));//dwhydm;//单位行业代码		
			$student->setDwszddm($this->areaRepository->findByUid($infoJob->return->dwszddm));	//dwszddm;//单位所在地代码 /dwszd;//单位所在地		
			$student->setGzzwlbdm($this->jobCategoryRepository->findByUid($infoJob->return->gzzwlbdm));	//gzzwlbdm;//工作职位类别代码	
			$student->setDwlxr($infoJob->return->dwlxr);										//dwlxr;//单位联系人		
			$student->setLxrdh($infoJob->return->lxrdh);										//lxrdh;//联系人电话		
			$student->setLxrsj($infoJob->return->lxrsj);										//lxrsj;//联系人手机		
			$student->setLxrdzyx($infoJob->return->lxrdzyx);									//lxrdzyx;//联系人电子邮箱		
			$student->setLxrcz($infoJob->return->lxrcz);										//lxrcz;//联系人传真		
			$student->setDwdz($infoJob->return->dwdz);											//dwdz;//单位地址		
			$student->setDwyb($infoJob->return->dwyb);											//dwyb;//单位邮编		
			$student->setBdzqflbdm($this->bdzqflbRepository->findByUid($infoJob->return->bdzqflbdm));//bdzqflbdm;//报到证签发类别代码	
			$student->setBdzqwdwmc($infoJob->return->bdzqwdwmc);								//bdzqwdwmc;//报到证签往单位名称	
			$student->setQwdwszddm($this->areaRepository->findByUid($infoJob->return->qwdwszddm));//qwdwszddm;//签往单位所在地代码 //qwdwszd;//签往单位所在地		
			$student->setDazjdwmc($infoJob->return->dazjdwmc); 									//dazjdwmc;//档案转寄单位名称	
			$student->setDazjdwdz($infoJob->return->dazjdwdz); 									//dazjdwdz;//档案转寄单位地址	
			$student->setDazjdwyb($infoJob->return->dazjdwyb); 									//dazjdwyb;//档案转寄单位邮编	
			$student->setHkqzdz($infoJob->return->hkqzdz); 										//hkqzdz;//户口迁转地址
			$student->setIsjobchecked($infoJob->return->ischecked == 'true' ? true : $infoJob->return->ischecked);							//ischecked;//是否被确认
			$student->setIssynced(2); 
		}
	}
	
	/**
	 * @return void
	 */
	public function initializeObject() {
	
		$this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->jobRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\JobRepository');
		$this->collegeRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\CollegeRepository');
		$this->majorRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\MajorRepository');
		$this->genderRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\GenderRepository');
		$this->jobCategoryRepository = $this->objectManager->get('\TaoJiang\SchoolJobs\Domain\Repository\JobCategoryRepository');
		$this->nationalRepository = $this->objectManager->get('\TaoJiang\SchoolJobs\Domain\Repository\NationalRepository');
		$this->politicalRepository = $this->objectManager->get('\TaoJiang\SchoolJobs\Domain\Repository\PoliticalRepository');
		$this->schoolRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\SchoolRepository');
		$this->diplomaRepository = $this->objectManager->get('\TaoJiang\SchoolJobs\Domain\Repository\DiplomaRepository');
		$this->pyfsRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\PyfsRepository');
		$this->areaRepository = $this->objectManager->get('\TaoJiang\SchoolJobs\Domain\Repository\AreaRepository');
		$this->schoolsystemRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\SchoolsystemRepository');
		$this->sfslbRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\SfslbRepository');
		$this->knslbRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\KnslbRepository');
		$this->sfmyjxRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\SfmyjxRepository');
		$this->companyPropertyRepository = $this->objectManager->get('\TaoJiang\SchoolJobs\Domain\Repository\CompanyPropertyRepository');
		$this->industryRepository = $this->objectManager->get('\TaoJiang\SchoolJobs\Domain\Repository\IndustryRepository');
		$this->bdzqflbRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\BdzqflbRepository');
		$this->byqxRepository = $this->objectManager->get('\TaoJiang\SchoolAgreement\Domain\Repository\ByqxRepository');
		$this->persistenceManager = $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface');
	}
	
}