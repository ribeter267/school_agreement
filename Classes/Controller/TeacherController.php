<?php
namespace TaoJiang\SchoolAgreement\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * StudentController
 */
class TeacherController extends CommonController {

	/**
	 * 学院管理员用户列表
	 * @return void
	 */
	public function listAction() {
		$search = $this->request->hasArgument('search') ? $this->request->getArgument('search') : array();
		$teachers = $this->teacherRepository->findBySchool($this->settings['schoolGroups'],$search);
		$this->view->assign('teachers', $teachers);
		$this->view->assign('schools', $this->schoolRepository->findAll());
		$this->view->assign('pageUid', $GLOBALS['TSFE']->id);
		$this->view->assign('search', $search);
		$this->view->assign('returnUrl', \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
	}
	
	/**
	 * 学院管理员信息查看
	 * @return void
	 */
	public function showAction(\TaoJiang\SchoolAgreement\Domain\Model\Teacher $teacher) {
	}
	
	/**
	 * 学院管理员信息编辑
	 * @return void
	 */
	public function editAction(\TaoJiang\SchoolAgreement\Domain\Model\Teacher $teacher) {

		$this->view->assign('teacher', $teacher);
		$this->view->assign('schools', $this->schoolRepository->findAll());
		if($this->request->hasArgument('returnUrl')) $this->view->assign('returnUrl',$this->request->getArgument('returnUrl'));
	}
	
	
	/**
	 * 学院管理员信息更新
	 * @return void
	 */
	public function updateAction(\TaoJiang\SchoolAgreement\Domain\Model\Teacher $teacher) {
	
		$this->addFlashMessage('更新成功');
		$this->teacherRepository->update($teacher);
		if($this->request->hasArgument('returnUrl')) $this->redirectToUri($this->request->getArgument('returnUrl'));
        else $this->redirect('list');
	}
	
	
	/**
	 * 学院管理员信息删除
	 * @return void
	 */
	public function deleteAction() {
	
		$teacher = $this->request->hasArgument('teacher') ? $this->request->getArgument('teacher') : '';
		if($teacher != ''){
			$this->addFlashMessage('删除成功');
			$this->teacherRepository->deleteByUidstring($teacher);
		}else{
			$this->addFlashMessage('用户信息不存在');
		}
		
		//$this->teacherRepository->remove($teacher);
		if($this->request->hasArgument('returnUrl')) $this->redirectToUri($this->request->getArgument('returnUrl'));
        else $this->redirect('list');
	}
	
	
	/**
	 * 学院管理员信息激活
	 * @return void
	 */
	public function activeAction() {
	
		
		
		$teacher = $this->request->hasArgument('teacher') ? $this->request->getArgument('teacher') : '';
		
		if($teacher != ''){
			$this->addFlashMessage('激活成功');
			$this->teacherRepository->activeByUidstring($teacher);
		}else{
			$this->addFlashMessage('用户信息不存在');
		}
		
		if($this->request->hasArgument('returnUrl')) $this->redirectToUri($this->request->getArgument('returnUrl'));
        else $this->redirect('list');
	}
	
	
	/**
	 * 学院管理员信息批量激活
	 * @return void
	 */
	public function multiactiveAction() {
	
		$items = $this->request->hasArgument('datas') ? $this->request->getArgument('datas') : array();
		if($items['items']){
			$item =  substr($items['items'], 0, strlen($items['items']) - 1);
			$this->teacherRepository->activeByUidstring($item);
			
			$this->addFlashMessage('激活成功');
			$this->redirect('list');
		}
		
		$this->addFlashMessage('没有可激活对象');
		
		if($this->request->hasArgument('returnUrl')) $this->redirectToUri($this->request->getArgument('returnUrl'));
        else $this->redirect('list');
	}
	
	
	/**
	 * 学院管理员信息批量删除
	 * @return void
	 */
	public function multideleteAction() {
	
		$items = $this->request->hasArgument('datas') ? $this->request->getArgument('datas') : array();
		if($items['items']){
			$item =  substr($items['items'], 0, strlen($items['items']) - 1);
			$this->teacherRepository->deleteByUidstring($item);
			
			$this->addFlashMessage('删除成功');
			$this->redirect('list');
		}
		
		$this->addFlashMessage('没有可删除对象');
	
		if($this->request->hasArgument('returnUrl')) $this->redirectToUri($this->request->getArgument('returnUrl'));
        else $this->redirect('list');
	}
	
	
}