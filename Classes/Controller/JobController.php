<?php
namespace TaoJiang\SchoolAgreement\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * JobController
 */
class JobController extends CommonController {

	/**
	 * action show
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Job $job
	 * @return void
	 */
	public function showAction(\TaoJiang\SchoolAgreement\Domain\Model\Job $job) {
		$this->view->assign('job', $job);
	}

	
	/**
	 * action edit
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Job $job
	 * @ignorevalidation $job
	 * @return void
	 */
	public function editAction(\TaoJiang\SchoolAgreement\Domain\Model\Job $job = null) {
		$job = $job ? : $this->jobRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
		//debug($job,'$job');
		$this->view->assign('job', $job);
		$this->view->assign('companyProperties', $this->companyPropertyRepository->findAll());
		$this->view->assign('industries', $this->industryRepository->findAll());
		$this->view->assign('areas', $this->areaRepository->findAll());
		$this->view->assign('jobCategories', $this->jobCategoryRepository->findAll());
		$this->view->assign('byqxs', $this->byqxRepository->findAll());
		$this->view->assign('bdzqflbs', $this->bdzqflbRepository->findAll());
	}

	/**
	 * action update
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Job $job
	 * @return void
	 */
	public function updateAction(\TaoJiang\SchoolAgreement\Domain\Model\Job $job) {
		$this->addFlashMessage('更新成功');
		$this->setRemoteData($job);
		$this->jobRepository->update($job);
		$this->redirect('edit');
	}
	
	/**
	 * 编辑远程数据
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Student $student
	 * @return void
	 */
	protected function setRemoteData(\TaoJiang\SchoolAgreement\Domain\Model\Job $job){
	
		//基础信息核对
		$jobinfo = array(
			'bdzqflbdm' => $job->getBdzqflbdm() ? $job->getBdzqflbdm()->getUid() : '',
			'bdzqwdwmc' => $job->getBdzqwdwmc(),
			'byqxdm' => $job->getByqxdm() ? $job->getByqxdm()->getUid() : '',
			'bz' => $job->getJobBz(),
			'dazjdwdz' => $job->getDazjdwdz(),
			'dazjdwmc' => $job->getDazjdwmc(),
			'dazjdwyb' => $job->getDazjdwyb(),
			'dwdz' => $job->getDwdz(),
			'dwhydm' => $job->getDwhydm() ? $job->getDwhydm()->getUid() : '',
			'dwlxr' => $job->getDwlxr(),
			'dwmc' => $job->getDwmc(),
			'dwszd' => $job->getDwszddm() ? $job->getDwszddm()->getTitle() : '',
			'dwszddm' => $job->getDwszddm() ? $job->getDwszddm()->getUid() : '',
			'dwxzdm' => $job->getDwxzdm() ? $job->getDwxzdm()->getUid() : '',
			'dwyb' => $job->getDwyb(),
			'dwzzjgdm' => $job->getDwzzjgdm(),
			'gzzwlbdm' => $job->getGzzwlbdm() ? $job->getGzzwlbdm()->getUid() : '',
			'hkqzdz' => $job->getHkqzdz(),
			'ksh' => $job->getKsh(),
			'lxrcz' => $job->getLxrcz(),
			'lxrdh' => $job->getLxrdh(),
			'lxrdzyx' => $job->getLxrdzyx(),
			'lxrsj' => $job->getLxrsj(),
			'qwdwszd' => $job->getQwdwszddm() ? $job->getQwdwszddm()->getTitle() : '',
			'qwdwszddm' => $job->getQwdwszddm() ? $job->getQwdwszddm()->getUid() : '',
			'sfzh' => $job->getIdCard(),
			'xh' => $job->getNumber(),
			'xm' => $job->getName(),

		);
		$this->soapClient()->setJobs(array('arg0'=>$jobinfo));
	}

}