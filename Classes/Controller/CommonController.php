<?php
namespace TaoJiang\SchoolAgreement\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJiang <ribeter267@gmail.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * StudentController
 */
class CommonController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;
	
	
	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\BdzqflbRepository
	 * @inject
	 */
	protected $bdzqflbRepository;
	
	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\ByqxRepository
	 * @inject
	 */
	protected $byqxRepository;
	
	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\GenderRepository
	 * @inject
	 */
	protected $genderRepository;
	
	/**
	 * @var \TaoJiang\SchoolJobs\Domain\Repository\JobCategoryRepository
	 * @inject
	 */
	protected $jobCategoryRepository;
	
	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\JobRepository
	 * @inject
	 */
	protected $jobRepository;
	
	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\KnslbRepository
	 * @inject
	 */
	protected $knslbRepository;
	
	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\PyfsRepository
	 * @inject
	 */
	protected $pyfsRepository;
	
	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\SchoolsystemRepository
	 * @inject
	 */
	protected $schoolsystemRepository;
	
	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\SfmyjxRepository
	 * @inject
	 */
	protected $sfmyjxRepository;
	
	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\SfslbRepository
	 * @inject
	 */
	protected $sfslbRepository;

	
	/**
	 * @var \TaoJiang\SchoolJobs\Domain\Repository\NationalRepository
	 * @inject
	 */
	protected $nationalRepository;

	/**
	 * @var \TaoJiang\SchoolJobs\Domain\Repository\PoliticalRepository
	 * @inject
	 */
	protected $politicalRepository;

	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\SchoolRepository
	 * @inject
	 */
	protected $schoolRepository;

	/**
	 * @var \TaoJiang\SchoolJobs\Domain\Repository\DiplomaRepository
	 * @inject
	 */
	protected $diplomaRepository;

	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\MajorRepository
	 * @inject
	 */
	protected $majorRepository;

	/**
	 * @var \TaoJiang\SchoolJobs\Domain\Repository\AreaRepository
	 * @inject
	 */
	protected $areaRepository;

	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\CollegeRepository
	 * @inject
	 */
	protected $collegeRepository;

	/**
	 * @var \TaoJiang\SchoolJobs\Domain\Repository\CompanyPropertyRepository
	 * @inject
	 */
	protected $companyPropertyRepository;

	/**
	 * @var \TaoJiang\SchoolJobs\Domain\Repository\IndustryRepository
	 * @inject
	 */
	protected $industryRepository;


	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\StudentRepository
	 * @inject
	 */
	protected $studentRepository;
	
	/**
	 * @var \TaoJiang\SchoolAgreement\Domain\Repository\TeacherRepository
	 * @inject
	 */
	protected $teacherRepository;
	
	
	
	/**
	 * soap 客户端初始化
	 * @return soap
	 */
	public function soapClient(){
		try{
			$client = new \SoapClient($this->settings['soap']['serverUrl'], array(
				'encoding'=>'utf-8',
				'login' => $this->settings['soap']['serverUser'],
				'password' => $this->settings['soap']['serverPass'],
				'trace' => 1,
				'exception' => 1
			));
			return $client;
			
		}catch(SoapFault $e){
			var_dump($e);
		}catch(Exception $e){
			var_dump($e);
		}
	}
}