<?php
namespace TaoJiang\SchoolAgreement\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * StudentController
 */
class StudentController extends CommonController {

	/**
	 * action list
	 * 
	 * @return void
	 */
	public function listAction() {
		$students = $this->jobRepository->findAll();
		$this->view->assign('students', $students);
	}
	
	
	/**
	 * 签约统计
	 * @return void
	 */
	public function statisticsAction() {
	
		$search = $this->request->hasArgument('search') ? $this->request->getArgument('search') : array();
		
		if(\TYPO3\CMS\Core\Utility\GeneralUtility::inList($GLOBALS['TSFE']->fe_user->user['usergroup'],$this->settings['schoolGroups'])){
			$teacher = $this->teacherRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
			if($teacher->getSchool() == null) return '学校未分配，请管理员在后台指定您所在的学校!';
			
			$search['school'] = $teacher->getSchool()->getUid();
			$this->view->assign('schoolUser', true);
			$this->view->assign('colleges', $this->collegeRepository->findAllByXydm($teacher->getSchool()->getUid()));
			if($search['college']) $this->view->assign('majors', 
					$this->majorRepository->findAllByXydm($teacher->getSchool()->getUid(),
					$this->collegeRepository->findByUid($search['college'])->getYzsh()));
		}
		
		
		$students = $this->jobRepository->findAllAgreementStudent($this->settings['studentGroups'],$search);
		$this->view->assign('students', $students);
		$this->view->assign('diplomas', $this->diplomaRepository->findAll());
		$this->view->assign('schools', $this->schoolRepository->findAll());
		$this->view->assign('lddms', $this->jobRepository->findAllLddms());
		$this->view->assign('pageUid', $GLOBALS['TSFE']->id);
		$this->view->assign('search', $search);
	}
	

	/**
	 * 学生详细信息
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Job $student
	 * @return void
	 */
	public function showAction(\TaoJiang\SchoolAgreement\Domain\Model\Job $student) {
		$this->view->assign('student', $student);
	}
	
	

	/**
	 * 基础信息核对
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Student $student
	 * @ignorevalidation $student
	 * @return void
	 */
	public function editAction(\TaoJiang\SchoolAgreement\Domain\Model\Student $student = null) {
		
		$student = $student ? : $this->studentRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
		$this->view->assign('student', $student);
	}

	/**
	 * 基础信息更新
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Student $student
	 * @return void
	 */
	public function updateAction(\TaoJiang\SchoolAgreement\Domain\Model\Student $student) {
	
		
		$this->addFlashMessage('保存成功');
		$this->setRemoteData($student);
		$this->studentRepository->update($student);
		$this->redirect('edit');
	}

	
	/**
	 * 打印
	 * @return void
	 */
	public function printAction() {
		$student = $this->jobRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
		
		if($student->getIsjobchecked()) return '协议书已通过审核，不能再打印';
		
		$this->view->assign('student', $student);
	}
	
	
	/**
	 * 批量打印
	 * @param string $studentIds
	 * @reutrn void
	 */
	public function multiprintAction($studentIds = ''){
		
		return '功能未开启';
	}
	
	
	/**
	 * 自动打印
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Student $student
	 * @return void
	 */
	public function autoprintAction(\TaoJiang\SchoolAgreement\Domain\Model\Student $student){
		
		$this->view->assign('student', $student);
	}
	

	/**
	 * 信息绑定表单
	 * @return void
	 */
	public function bindAction(\TaoJiang\SchoolAgreement\Domain\Model\Job $student = null) {
		
		//debug($student,'$student');
		$student = $student ? $student : $this->jobRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
		$this->view->assign('student', $student);
	}

	/**
	 * 信息绑定更新
	 * @return void
	 */
	public function bindupAction(\TaoJiang\SchoolAgreement\Domain\Model\Job $student) {

		$info = $this->soapClient()->getSources(array('arg0'=>$student->getIdCard()));
		
		if($binder = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('username,email', 'fe_users', 'id_card = "'.$student->getIdCard().'" AND issynced > 0')){
		
			$this->addFlashMessage('该信息已经被 '.$binder['username'].' ('.$binder['email'].') 绑定。请确认你的信息是否填写正确。');
		}else if($info->return == null){
		
			$this->addFlashMessage('无对应信息，请检查身份证号是否有误');
		//}else if($info->return->ksh != $student->getKsh()){
		//	$this->addFlashMessage('绑定失败，请检查考生号是否有误');
		}else if($info->return->xm != $student->getName()){
		
			$this->addFlashMessage('绑定失败，姓名信息不匹配');
		}else{
		
			$this->getRemoteData($student); //获取远程数据
			$this->addFlashMessage('绑定成功!');
		}
		$this->studentRepository->update($student);
		
		//redirect($actionName, $controllerName = NULL, $extensionName = NULL, array $arguments = NULL, $pageUid = NULL, $delay = 0, $statusCode = 303)
		$this->redirect('bind',NULL,NULL,array('student' => $student));
	}
	
	
	/**
	 * 获取远程数据, 并合并
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Student $student
	 * return void
	 */
	protected function getRemoteData(\TaoJiang\SchoolAgreement\Domain\Model\Job $student){
		
		
		//基础信息
		$info = $this->soapClient()->getSources(array('arg0'=>$student->getIdCard()));
		if($info->return == null) return false;
		
		$student->setLddm($info->return->lddm); 											//lddm;//毕业年度
		$student->setXslx($info->return->xslx); 											//xslx;//学生类别：0：本专科，1：研究生
		
		$student->setSchool($this->schoolRepository->findByUid($info->return->yxdm));		//yxdm;//院校代码 //yxmc;//院校名称	
		$student->setCollege($this->collegeRepository->findByXydm($info->return->yxdm,$info->return->xydm)); 	//xydm;//学院代码 //xymc;//学院名称
		$student->setMajor($this->majorRepository->findByZydm($info->return->yxdm,$info->return->xydm,$info->return->zydm)); 		//zymc;//专业名称 //zydm;//专业代码	 //zy;//专业		
		$student->setClass($info->return->bjbh);											//bjbh;//班级
		$student->setSchoolName($info->return->yxmc);
		$student->setCollegeName($info->return->xymc);
		$student->setMajorName($info->return->zymc);
		
		
		$student->setKsh($info->return->ksh);												//ksh;//考生号		
		$student->setIdCard($info->return->sfzh);											//sfzh;//身份证号	
		$student->setName($info->return->xm);												//xm;//姓名
		$student->setGender($this->genderRepository->findByUid($info->return->xbdm));		//xbdm;//性别代码
		$student->setNational($this->nationalRepository->findByUid($info->return->mzdm));		//mzdm;//民族代码	
		$student->setPolitical($this->politicalRepository->findByUid($info->return->zzmmdm));//zzmmdm;//政治面貌代码	
		$student->setYxszsdm($info->return->yxszsdm);										//yxszsdm;//院校所在省代码	
		$student->setDiploma($this->diplomaRepository->findByUid($info->return->xldm));		//xldm;//学历代码	
		$student->setZyfx($info->return->zyfx);												//zyfx;//专业方向	
		$student->setPyfsdm($this->pyfsRepository->findByUid($info->return->pyfsdm));		//pyfsdm;//培养方式代码	
		$student->setNativetowns($this->areaRepository->findByUid($info->return->syszddm));	//syszddm;//生源所在地代码 //syszd;//生源所在地	
		$student->setSchoolsystem($this->schoolsystemRepository->findByUid($info->return->xz));	//xz;//学制		
		$student->setRxsj(new \DateTime($info->return->rxsj));								//rxsj;//入学时间	
		$student->setBysj(new \DateTime($info->return->bysj));								//bysj;//毕业时间	
		$student->setSfslbdm($this->sfslbRepository->findByUid($info->return->sfslbdm));	//sfslbdm;//师范生类别代码	
		$student->setKnslbdm($this->knslbRepository->findByUid($info->return->knslbdm));	//knslbdm;//困难生类别代码	
		$student->setDxhwpdw($info->return->dxhwpdw);										//dxhwpdw;//定向或委培单位	
		$student->setCampus($info->return->fxmc);											//fxmc;//分校名称	
		$student->setNumber($info->return->xh);												//xh;//学号
		$student->setBirthday(new \DateTime($info->return->csrq));							//csrq;//出生日期
		$student->setXysdm($info->return->xysdm);											//	xysdm;//协议书代码
		$student->setSfmyjx($this->sfmyjxRepository->findByUid($info->return->sfmyjx));		//	sfmyjx;//授课语言
		$student->setZsbh($info->return->zsbh);												//	zsbh;//证书编号
		//	bz;//备注
		//	csjyshdm;//初始化就业信息代码审核是否通过
		$student->setIsimp($info->return->isimp == 'true' ? true : $info->return->isimp);	//	isimp;//是否导入就业方案
		$student->setXysff($info->return->xysff == 'true' ? true : $info->return->xysff);	//	xysff;//协议书发放
		$student->setErrinfo($info->return->errinfo);										//	errinfo;//审核错误信息
		$student->setIssourcechecked($info->return->ischecked == 'true' ? true : $info->return->ischecked);	//	ischecked;//是否被确认
		$student->setIssynced(1); 															//基础信息已同步
		$student->setLocalGraduate(true);													//省内毕业生
		
		//扩展信息
		$infoPlus = $this->soapClient()->getSourceExtend(array('arg0'=>$student->getIdCard()));
		if($infoPlus->return != null){
			$student->setCxsy($infoPlus->return->cxsy);										//cxsy;//城乡生源		
			$student->setRxqdaszdw($infoPlus->return->rxqdaszdw);							//rxqdaszdw;//入学前档案所在单位	
			$student->setDasfzrxx($infoPlus->return->dasfzrxx == 'true' ? true : $infoPlus->return->dasfzrxx);	//dasfzrxx;//档案是否转入学校	
			$student->setRxqhkszdpcs($infoPlus->return->rxqhkszdpcs);						//rxqhkszdpcs;//入学前户口所在地派出所	
			$student->setHksfzrxx($infoPlus->return->hksfzrxx == 'true' ? true : $infoPlus->return->hksfzrxx);		//hksfzrxx;//户口是否转入学校	
			//$student->setMobile($infoPlus->return->sjhm);									//sjhm;//手机号码		
			//$student->setEmail($infoPlus->return->dzxx);									//dzxx;//电子邮箱		
			$student->setQq($infoPlus->return->qqhm);										//qqhm;//QQ号码			
			$student->setHomeAddress($infoPlus->return->jtdz);								//jtdz;//家庭地址		
			$student->setHomeTelephone($infoPlus->return->jtdh);							//jtdh;//家庭电话		
			$student->setHomeZip($infoPlus->return->jtyb);								//jtyb;//家庭邮编	
			
			if($student->getMobile() == ''){
				$student->setMobile($infoPlus->return->sjhm);									//sjhm;//手机号码		
			}
			if($student->getEmail() == ''){
				$student->setEmail($infoPlus->return->dzxx);									//dzxx;//电子邮箱	
			}
		}
		
		//就业方案信息
		$infoJob = $this->soapClient()->getJobs(array('arg0'=>$student->getIdCard()));
		if($infoJob->return != null){
		
			$student->setByqxdm($this->byqxRepository->findByUid($infoJob->return->byqxdm));  //byqxdm;//毕业去向代码		
			$student->setDwmc($infoJob->return->dwmc);											//dwmc;//单位名称		
			$student->setDwzzjgdm($infoJob->return->dwzzjgdm);									//dwzzjgdm;//单位组织机构代码	
			$student->setDwxzdm($this->companyPropertyRepository->findByUid($infoJob->return->dwxzdm));//dwxzdm;//单位性质代码		
			$student->getDwhydm($this->industryRepository->findByUid($infoJob->return->dwhydm));//dwhydm;//单位行业代码		
			$student->setDwszddm($this->areaRepository->findByUid($infoJob->return->dwszddm));	//dwszddm;//单位所在地代码 /dwszd;//单位所在地		
			$student->setGzzwlbdm($this->jobCategoryRepository->findByUid($infoJob->return->gzzwlbdm));	//gzzwlbdm;//工作职位类别代码	
			$student->setDwlxr($infoJob->return->dwlxr);										//dwlxr;//单位联系人		
			$student->setLxrdh($infoJob->return->lxrdh);										//lxrdh;//联系人电话		
			$student->setLxrsj($infoJob->return->lxrsj);										//lxrsj;//联系人手机		
			$student->setLxrdzyx($infoJob->return->lxrdzyx);									//lxrdzyx;//联系人电子邮箱		
			$student->setLxrcz($infoJob->return->lxrcz);										//lxrcz;//联系人传真		
			$student->setDwdz($infoJob->return->dwdz);											//dwdz;//单位地址		
			$student->setDwyb($infoJob->return->dwyb);											//dwyb;//单位邮编		
			$student->setBdzqflbdm($this->bdzqflbRepository->findByUid($infoJob->return->bdzqflbdm));//bdzqflbdm;//报到证签发类别代码	
			$student->setBdzqwdwmc($infoJob->return->bdzqwdwmc);								//bdzqwdwmc;//报到证签往单位名称	
			$student->setQwdwszddm($this->areaRepository->findByUid($infoJob->return->qwdwszddm));//qwdwszddm;//签往单位所在地代码 //qwdwszd;//签往单位所在地		
			$student->setDazjdwmc($infoJob->return->dazjdwmc); 									//dazjdwmc;//档案转寄单位名称	
			$student->setDazjdwdz($infoJob->return->dazjdwdz); 									//dazjdwdz;//档案转寄单位地址	
			$student->setDazjdwyb($infoJob->return->dazjdwyb); 									//dazjdwyb;//档案转寄单位邮编	
			$student->setHkqzdz($infoJob->return->hkqzdz); 										//hkqzdz;//户口迁转地址
			$student->setIsjobchecked($infoJob->return->ischecked == 'true' ? true : $infoJob->return->ischecked);							//ischecked;//是否被确认
			$student->setIssynced(2); 
		}
	}
	
	
	/**
	 * 编辑远程数据
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Student $student
	 * @return void
	 */
	protected function setRemoteData(\TaoJiang\SchoolAgreement\Domain\Model\Student $student){
	
		//基础信息核对
		$source = array(
			'arg0' => $student->getIdCard() ? : $student->getKsh(),
			'arg1' => $student->getIssourcechecked(),
			'arg2' => $student->getErrinfo(),
		);
		$this->soapClient()->submitSource($source);
		
		//扩展信息
		$extinfo = array(
			'bz' => $student->getSourceBz(),
			'cxsy' => $student->getCxsy(),
			'dasfzrxx' => $student->getDasfzrxx(),
			'dzxx' => $student->getEmail(),
			'hksfzrxx' => $student->getHksfzrxx(),
			'jtdh' => $student->getHomeTelephone(),
			'jtdz' => $student->getHomeAddress,
			'jtyb' => $student->getHomeZip(),
			'ksh' => $student->getKsh(),
			'qqhm'	=> $student->getQq(),
			'rxqdaszdw' => $student->getRxqdaszdw(),
			'rxqhkszdpcs' => $student->getRxqhkszdpcs(),
			'sfzh' => $student->getIdCard(),
			'sjhm' => $student->getMobile(),
			'xh' => $student->getNumber(),
			'xm' => $student->getName(),
		);
		$this->soapClient()->setSourceExtend(array('arg0'=>$extinfo));
	}
}