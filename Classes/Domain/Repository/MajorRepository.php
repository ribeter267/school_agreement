<?php
namespace TaoJiang\SchoolAgreement\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Majors
 */
class MajorRepository extends CommonRepository {

	/**
	 * 根据院校，学院， 查找专业信息
	 * 学院代码可能重复
	 * @param int $yxdm 院校代码
	 * @param string $xydm 学院代码
	 */
	public function findByZydm($yxdm,$xydm,$zydm){
		$query = $this->createQuery();
		
		$condition = array(
			$query->equals('xxdm',$yxdm),
			$query->equals('yzsh',$xydm),
			$query->equals('zyh',$zydm),
		);

        $query->matching($query->logicalAnd(
			$condition));
			
		//$query->setOrderings(array(
		//	'status' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
		//	'uid' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
		//));
		//$GLOBALS['TYPO3_DB']->debugOutput = 2;
		
        $result = $query->execute()->getFirst();
        return $result;
	}
	
	
	
	/**
	 * 根据 院校， 学院 查找所有专业信息
	 * 学院代码可能重复
	 * @param int $yxdm 院校代码
	 * @param string $xydm 学院代码
	 */
	public  function findAllByXydm($yxdm,$xydm){
	
		$query = $this->createQuery();
		
		$condition = array(
			$query->equals('xxdm',$yxdm),
			$query->equals('yzsh',$xydm),
		);

        $query->matching($query->logicalAnd(
			$condition));
	
        $result = $query->execute();
        return $result;
	}
	
}