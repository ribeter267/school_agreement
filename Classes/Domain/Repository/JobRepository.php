<?php
namespace TaoJiang\SchoolAgreement\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Genders
 */
class JobRepository extends CommonRepository {
	
	/**
	 * 查找所有已同步记录的学生
	 * @param int $lddm 毕业年份
	 * @return object
	 */
	public function findIssyncedStudent($lddm){
		
		$query = $this->createQuery();
		
		$condition = array(
			$query->greaterThanOrEqual('lddm',$lddm),
			$query->greaterThanOrEqual('issynced',1),
		);

        $query->matching($query->logicalAnd(
			$condition));

        $result = $query->execute();
        return $result;

	}
	
	
	/**
	 * 查找所有学生
	 * @param int $studentGroup
	 * @return object
	 */
	public function findAllAgreementStudent($studentGroup,$search = NULL){
	
		$query = $this->createQuery();
		$condition = array(
			$query->contains('usergroup',$studentGroup),
			$query->greaterThanOrEqual('issynced',1),
		);
		
		if($search != null){
			if($search['lddm']) $condition[] = $query->equals('lddm',$search['lddm']);
			if($search['school']) $condition[] = $query->equals('school.uid',$search['school']);
			if($search['diploma']) $condition[] = $query->equals('diploma.uid',$search['diploma']);
			if($search['school']) $condition[] = $query->equals('school.uid',$search['school']);
			if($search['college']) $condition[] = $query->equals('college.uid',$search['college']);
			if($search['major']) $condition[] = $query->equals('major.uid',$search['major']);
			if($search['keyword']){
				$condition[] = $query->logicalOr(
					array(
						$query->like('name','%'.$search['keyword'].'%'),
						$query->like('idCard','%'.$search['keyword'].'%'),
				));
			}
		}

        $query->matching($query->logicalAnd(
			$condition));

        $result = $query->execute();
        return $result;
	}
	
	
	/**
	 * 获取所有的毕业年份
	 */
	public function findAllLddms(){
	
		$output = array();
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('lddm' , 'fe_users', 'lddm > 0', 'lddm','lddm DESC');
		if (!$GLOBALS['TYPO3_DB']->sql_error()) {
			while ($tempRow = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
				$output[$tempRow['lddm']] = $tempRow['lddm'];
			}
		}
		return $output;
	}
}