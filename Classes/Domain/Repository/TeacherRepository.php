<?php
namespace TaoJiang\SchoolAgreement\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Genders
 */
class TeacherRepository extends CommonRepository {
	
	/**
	 * 查找学院帐号
	 * @teachergroup int
	 * @search array
	 */
	public function findBySchool($teachergroup, $search){
		$query = $this->createQuery();
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		
		$condition = array(
			$query->contains('usergroup',$teachergroup),
			//$query->greaterThanOrEqual('issynced',1),
		);
		
		if($search != null){
			if($search['school']) $condition[] = $query->equals('school.uid',$search['school']);
			if($search['keyword']){
				$condition[] = $query->logicalOr(
					array(
						$query->like('name','%'.$search['keyword'].'%'),
						$query->like('username','%'.$search['keyword'].'%'),
						$query->like('mobile','%'.$search['keyword'].'%'),
						$query->like('email','%'.$search['keyword'].'%'),
				));
			}
		}

        $query->matching($query->logicalAnd($condition));
        $result = $query->execute();
        return $result;
	}
	
	
	/**
	 * 删除一组数据
	 * @param string $uids
	 * return void
	 */
	public function deleteByUidstring($uids){
		
		return $GLOBALS['TYPO3_DB']->exec_DELETEquery('fe_users', 'uid in ('.$uids.')');
	}
	
	
	/**
	 * 删除一组数据
	 * @param string $uids
	 * return void
	 */
	public function activeByUidstring($uids){
		
		return $GLOBALS['TYPO3_DB']->exec_UPDATEquery('fe_users', 'uid in ('.$uids.')', array('disable' => 0));
	}
}