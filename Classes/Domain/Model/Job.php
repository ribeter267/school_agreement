<?php
namespace TaoJiang\SchoolAgreement\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 学生就业信息
 */
class Job extends Student {

	/**
	 * 单位名称
	 * 
	 * @var string
	 */
	protected $dwmc = '';

	/**
	 * 单位组织机构代码
	 * 
	 * @var string
	 */
	protected $dwzzjgdm = '';

	/**
	 * 单位联系人
	 * 
	 * @var string
	 */
	protected $dwlxr = '';

	/**
	 * 联系人电话
	 * 
	 * @var string
	 */
	protected $lxrdh = '';

	/**
	 * 联系人手机
	 * 
	 * @var string
	 */
	protected $lxrsj = '';

	/**
	 * 联系人电子邮箱
	 * 
	 * @var string
	 */
	protected $lxrdzyx = '';

	/**
	 * 联系人传真
	 * 
	 * @var string
	 */
	protected $lxrcz = '';

	/**
	 * 单位地址
	 * 
	 * @var string
	 */
	protected $dwdz = '';

	/**
	 * 单位邮编
	 * 
	 * @var string
	 */
	protected $dwyb = '';

	/**
	 * 报到证签往单位名称
	 * 
	 * @var string
	 */
	protected $bdzqwdwmc = '';

	/**
	 * 档案转寄单位名称
	 * 
	 * @var string
	 */
	protected $dazjdwmc = '';

	/**
	 * 档案转寄单位地址
	 * 
	 * @var string
	 */
	protected $dazjdwdz = '';

	/**
	 * 档案转寄单位邮编
	 * 
	 * @var string
	 */
	protected $dazjdwyb = '';

	/**
	 * 户口迁转地址
	 * 
	 * @var string
	 */
	protected $hkqzdz = '';

	/**
	 * 报到证编号
	 * 
	 * @var string
	 */
	protected $bdzbh = '';

	/**
	 * 报到起始时间
	 * 
	 * @var \DateTime
	 */
	protected $bdqssj = NULL;

	/**
	 * 就业方案信息备注
	 * 
	 * @var string
	 */
	protected $jobBz = '';

	/**
	 * 就业信息是否被确认
	 * 
	 * @var boolean
	 */
	protected $isjobchecked = FALSE;

	/**
	 * 毕业去向
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\Byqx
	 */
	protected $byqxdm = NULL;

	/**
	 * 单位所在地
	 * 
	 * @var \TaoJiang\SchoolJobs\Domain\Model\Area
	 */
	protected $dwszddm = NULL;

	/**
	 * 工作职位类别
	 * 
	 * @var \TaoJiang\SchoolJobs\Domain\Model\JobCategory
	 */
	protected $gzzwlbdm = NULL;

	/**
	 * 报到证签发类别
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\Bdzqflb
	 */
	protected $bdzqflbdm = NULL;

	/**
	 * qwdwszddm
	 * 
	 * @var \TaoJiang\SchoolJobs\Domain\Model\Area
	 */
	protected $qwdwszddm = NULL;

	/**
	 * 单位性质代码
	 * 
	 * @var \TaoJiang\SchoolJobs\Domain\Model\CompanyProperty
	 */
	protected $dwxzdm = NULL;

	/**
	 * 单位行业代码
	 * 
	 * @var \TaoJiang\SchoolJobs\Domain\Model\Industry
	 */
	protected $dwhydm = NULL;

	/**
	 * Returns the dwmc
	 * 
	 * @return string $dwmc
	 */
	public function getDwmc() {
		return $this->dwmc;
	}

	/**
	 * Sets the dwmc
	 * 
	 * @param string $dwmc
	 * @return void
	 */
	public function setDwmc($dwmc) {
		$this->dwmc = $dwmc;
	}

	/**
	 * Returns the dwzzjgdm
	 * 
	 * @return string $dwzzjgdm
	 */
	public function getDwzzjgdm() {
		return $this->dwzzjgdm;
	}

	/**
	 * Sets the dwzzjgdm
	 * 
	 * @param string $dwzzjgdm
	 * @return void
	 */
	public function setDwzzjgdm($dwzzjgdm) {
		$this->dwzzjgdm = $dwzzjgdm;
	}

	/**
	 * Returns the dwlxr
	 * 
	 * @return string $dwlxr
	 */
	public function getDwlxr() {
		return $this->dwlxr;
	}

	/**
	 * Sets the dwlxr
	 * 
	 * @param string $dwlxr
	 * @return void
	 */
	public function setDwlxr($dwlxr) {
		$this->dwlxr = $dwlxr;
	}

	/**
	 * Returns the lxrdh
	 * 
	 * @return string $lxrdh
	 */
	public function getLxrdh() {
		return $this->lxrdh;
	}

	/**
	 * Sets the lxrdh
	 * 
	 * @param string $lxrdh
	 * @return void
	 */
	public function setLxrdh($lxrdh) {
		$this->lxrdh = $lxrdh;
	}

	/**
	 * Returns the lxrsj
	 * 
	 * @return string $lxrsj
	 */
	public function getLxrsj() {
		return $this->lxrsj;
	}

	/**
	 * Sets the lxrsj
	 * 
	 * @param string $lxrsj
	 * @return void
	 */
	public function setLxrsj($lxrsj) {
		$this->lxrsj = $lxrsj;
	}

	/**
	 * Returns the lxrdzyx
	 * 
	 * @return string $lxrdzyx
	 */
	public function getLxrdzyx() {
		return $this->lxrdzyx;
	}

	/**
	 * Sets the lxrdzyx
	 * 
	 * @param string $lxrdzyx
	 * @return void
	 */
	public function setLxrdzyx($lxrdzyx) {
		$this->lxrdzyx = $lxrdzyx;
	}

	/**
	 * Returns the lxrcz
	 * 
	 * @return string $lxrcz
	 */
	public function getLxrcz() {
		return $this->lxrcz;
	}

	/**
	 * Sets the lxrcz
	 * 
	 * @param string $lxrcz
	 * @return void
	 */
	public function setLxrcz($lxrcz) {
		$this->lxrcz = $lxrcz;
	}

	/**
	 * Returns the dwdz
	 * 
	 * @return string $dwdz
	 */
	public function getDwdz() {
		return $this->dwdz;
	}

	/**
	 * Sets the dwdz
	 * 
	 * @param string $dwdz
	 * @return void
	 */
	public function setDwdz($dwdz) {
		$this->dwdz = $dwdz;
	}

	/**
	 * Returns the dwyb
	 * 
	 * @return string $dwyb
	 */
	public function getDwyb() {
		return $this->dwyb;
	}

	/**
	 * Sets the dwyb
	 * 
	 * @param string $dwyb
	 * @return void
	 */
	public function setDwyb($dwyb) {
		$this->dwyb = $dwyb;
	}

	/**
	 * Returns the bdzqwdwmc
	 * 
	 * @return string $bdzqwdwmc
	 */
	public function getBdzqwdwmc() {
		return $this->bdzqwdwmc;
	}

	/**
	 * Sets the bdzqwdwmc
	 * 
	 * @param string $bdzqwdwmc
	 * @return void
	 */
	public function setBdzqwdwmc($bdzqwdwmc) {
		$this->bdzqwdwmc = $bdzqwdwmc;
	}

	/**
	 * Returns the dazjdwmc
	 * 
	 * @return string $dazjdwmc
	 */
	public function getDazjdwmc() {
		return $this->dazjdwmc;
	}

	/**
	 * Sets the dazjdwmc
	 * 
	 * @param string $dazjdwmc
	 * @return void
	 */
	public function setDazjdwmc($dazjdwmc) {
		$this->dazjdwmc = $dazjdwmc;
	}

	/**
	 * Returns the dazjdwdz
	 * 
	 * @return string $dazjdwdz
	 */
	public function getDazjdwdz() {
		return $this->dazjdwdz;
	}

	/**
	 * Sets the dazjdwdz
	 * 
	 * @param string $dazjdwdz
	 * @return void
	 */
	public function setDazjdwdz($dazjdwdz) {
		$this->dazjdwdz = $dazjdwdz;
	}

	/**
	 * Returns the dazjdwyb
	 * 
	 * @return string $dazjdwyb
	 */
	public function getDazjdwyb() {
		return $this->dazjdwyb;
	}

	/**
	 * Sets the dazjdwyb
	 * 
	 * @param string $dazjdwyb
	 * @return void
	 */
	public function setDazjdwyb($dazjdwyb) {
		$this->dazjdwyb = $dazjdwyb;
	}

	/**
	 * Returns the hkqzdz
	 * 
	 * @return string $hkqzdz
	 */
	public function getHkqzdz() {
		return $this->hkqzdz;
	}

	/**
	 * Sets the hkqzdz
	 * 
	 * @param string $hkqzdz
	 * @return void
	 */
	public function setHkqzdz($hkqzdz) {
		$this->hkqzdz = $hkqzdz;
	}

	/**
	 * Returns the bdzbh
	 * 
	 * @return string $bdzbh
	 */
	public function getBdzbh() {
		return $this->bdzbh;
	}

	/**
	 * Sets the bdzbh
	 * 
	 * @param string $bdzbh
	 * @return void
	 */
	public function setBdzbh($bdzbh) {
		$this->bdzbh = $bdzbh;
	}

	/**
	 * Returns the bdqssj
	 * 
	 * @return \DateTime $bdqssj
	 */
	public function getBdqssj() {
		return $this->bdqssj;
	}

	/**
	 * Sets the bdqssj
	 * 
	 * @param \DateTime $bdqssj
	 * @return void
	 */
	public function setBdqssj(\DateTime $bdqssj) {
		$this->bdqssj = $bdqssj;
	}

	/**
	 * Returns the jobBz
	 * 
	 * @return string $jobBz
	 */
	public function getJobBz() {
		return $this->jobBz;
	}

	/**
	 * Sets the jobBz
	 * 
	 * @param string $jobBz
	 * @return void
	 */
	public function setJobBz($jobBz) {
		$this->jobBz = $jobBz;
	}

	/**
	 * Returns the isjobchecked
	 * 
	 * @return boolean $isjobchecked
	 */
	public function getIsjobchecked() {
		return $this->isjobchecked;
	}

	/**
	 * Sets the isjobchecked
	 * 
	 * @param boolean $isjobchecked
	 * @return void
	 */
	public function setIsjobchecked($isjobchecked) {
		$this->isjobchecked = $isjobchecked;
	}

	/**
	 * Returns the boolean state of isjobchecked
	 * 
	 * @return boolean
	 */
	public function isIsjobchecked() {
		return $this->isjobchecked;
	}

	/**
	 * Returns the byqxdm
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\Byqx $byqxdm
	 */
	public function getByqxdm() {
		return $this->byqxdm;
	}

	/**
	 * Sets the byqxdm
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Byqx $byqxdm
	 * @return void
	 */
	public function setByqxdm(\TaoJiang\SchoolAgreement\Domain\Model\Byqx $byqxdm = null) {
		$this->byqxdm = $byqxdm;
	}

	/**
	 * Returns the dwszddm
	 * 
	 * @return \TaoJiang\SchoolJobs\Domain\Model\Area $dwszddm
	 */
	public function getDwszddm() {
		return $this->dwszddm;
	}

	/**
	 * Sets the dwszddm
	 * 
	 * @param \TaoJiang\SchoolJobs\Domain\Model\Area $dwszddm
	 * @return void
	 */
	public function setDwszddm(\TaoJiang\SchoolJobs\Domain\Model\Area $dwszddm = null) {
		$this->dwszddm = $dwszddm;
	}

	/**
	 * Returns the gzzwlbdm
	 * 
	 * @return \TaoJiang\SchoolJobs\Domain\Model\JobCategory $gzzwlbdm
	 */
	public function getGzzwlbdm() {
		return $this->gzzwlbdm;
	}

	/**
	 * Sets the gzzwlbdm
	 * 
	 * @param \TaoJiang\SchoolJobs\Domain\Model\JobCategory $gzzwlbdm
	 * @return void
	 */
	public function setGzzwlbdm(\TaoJiang\SchoolJobs\Domain\Model\JobCategory $gzzwlbdm = null) {
		$this->gzzwlbdm = $gzzwlbdm;
	}

	/**
	 * Returns the bdzqflbdm
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\Bdzqflb $bdzqflbdm
	 */
	public function getBdzqflbdm() {
		return $this->bdzqflbdm;
	}

	/**
	 * Sets the bdzqflbdm
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Bdzqflb $bdzqflbdm
	 * @return void
	 */
	public function setBdzqflbdm(\TaoJiang\SchoolAgreement\Domain\Model\Bdzqflb $bdzqflbdm = null) {
		$this->bdzqflbdm = $bdzqflbdm;
	}

	/**
	 * Returns the qwdwszddm
	 * 
	 * @return \TaoJiang\SchoolJobs\Domain\Model\Area $qwdwszddm
	 */
	public function getQwdwszddm() {
		return $this->qwdwszddm;
	}

	/**
	 * Sets the qwdwszddm
	 * 
	 * @param \TaoJiang\SchoolJobs\Domain\Model\Area $qwdwszddm
	 * @return void
	 */
	public function setQwdwszddm(\TaoJiang\SchoolJobs\Domain\Model\Area $qwdwszddm = null) {
		$this->qwdwszddm = $qwdwszddm;
	}

	/**
	 * Returns the dwxzdm
	 * 
	 * @return \TaoJiang\SchoolJobs\Domain\Model\CompanyProperty $dwxzdm
	 */
	public function getDwxzdm() {
		return $this->dwxzdm;
	}

	/**
	 * Sets the dwxzdm
	 * 
	 * @param \TaoJiang\SchoolJobs\Domain\Model\CompanyProperty $dwxzdm
	 * @return void
	 */
	public function setDwxzdm(\TaoJiang\SchoolJobs\Domain\Model\CompanyProperty $dwxzdm = null) {
		$this->dwxzdm = $dwxzdm;
	}

	/**
	 * Returns the dwhydm
	 * 
	 * @return \TaoJiang\SchoolJobs\Domain\Model\Industry $dwhydm
	 */
	public function getDwhydm() {
		return $this->dwhydm;
	}

	/**
	 * Sets the dwhydm
	 * 
	 * @param \TaoJiang\SchoolJobs\Domain\Model\Industry $dwhydm
	 * @return void
	 */
	public function setDwhydm(\TaoJiang\SchoolJobs\Domain\Model\Industry $dwhydm = null) {
		$this->dwhydm = $dwhydm;
	}

}