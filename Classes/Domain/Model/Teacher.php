<?php
namespace TaoJiang\SchoolAgreement\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 院校管理老师
 */
class Teacher extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser {

	/**
	 * @var boolean
	 */
	protected $disable = FALSE;
	
	/**
	 * 所在院校
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\School
	 */
	protected $school = NULL;
	
	
	/**
	 * @var string
	 */
	protected $mobile = '';
	
	
	
	
	/**
	 * Returns the disable
	 * @return $disable
	 */
	public function getDisable() {
		return $this->disable;
	}
	
	
	/**
	 * @param boolean $disable
	 * @return void
	 */
	public function setDisable($disable) {
		return $this->disable = $disable;
	}
	
	/**
	 * Returns the school
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\School $school
	 */
	public function getSchool() {
		return $this->school;
	}
	
	
	/**
	 * Sets the school
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\School $school
	 * @return void
	 */
	public function setSchool(\TaoJiang\SchoolAgreement\Domain\Model\School $school) {
		$this->school = $school;
	}

	/**
	 * Returns the mobile
	 * @return string $mobile
	 */
	public function getMobile() {
		return $this->mobile;
	}
	
	
	/**
	 * Sets the mobile
	 * 
	 * @param string $mobile
	 * @return void
	 */
	public function setMobile($mobile = '') {
		$this->mobile = $mobile;
	}
}