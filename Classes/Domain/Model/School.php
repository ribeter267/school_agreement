<?php
namespace TaoJiang\SchoolAgreement\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 院校数据表
 */
class School extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * 院校名称
	 * 
	 * @var string
	 */
	protected $xxmc = '';

	/**
	 * 院校英文名称
	 * 
	 * @var string
	 */
	protected $xxywmc = '';

	/**
	 * Returns the xxmc
	 * 
	 * @return string $xxmc
	 */
	public function getXxmc() {
		return $this->xxmc;
	}

	/**
	 * Sets the xxmc
	 * 
	 * @param string $xxmc
	 * @return void
	 */
	public function setXxmc($xxmc) {
		$this->xxmc = $xxmc;
	}

	/**
	 * Returns the xxywmc
	 * 
	 * @return string $xxywmc
	 */
	public function getXxywmc() {
		return $this->xxywmc;
	}

	/**
	 * Sets the xxywmc
	 * 
	 * @param string $xxywmc
	 * @return void
	 */
	public function setXxywmc($xxywmc) {
		$this->xxywmc = $xxywmc;
	}

}