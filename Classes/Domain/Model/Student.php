<?php
namespace TaoJiang\SchoolAgreement\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 学生基础信息
 */
class Student extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser {

	/**
	 * 考生号
	 * 
	 * @var string
	 */
	protected $ksh = '';

	/**
	 * 身份证号
	 * 
	 * @var string
	 */
	protected $idCard = '';

	/**
	 * 院校所在省代码
	 * 
	 * @var string
	 */
	protected $yxszsdm = '';

	/**
	 * 专业方向
	 * 
	 * @var string
	 */
	protected $zyfx = '';

	/**
	 * 入学时间
	 * 
	 * @var \DateTime
	 */
	protected $rxsj = NULL;

	/**
	 * 毕业时间
	 * 
	 * @var \DateTime
	 */
	protected $bysj = NULL;

	/**
	 * 定向或委培单位
	 * 
	 * @var string
	 */
	protected $dxhwpdw = '';

	/**
	 * 分校名称
	 * 
	 * @var string
	 */
	protected $campus = '';

	/**
	 * 所在班级
	 * 
	 * @var string
	 */
	protected $class = '';

	/**
	 * 学号
	 * 
	 * @var string
	 */
	protected $number = '';

	/**
	 * 出生日期
	 * 
	 * @var \DateTime
	 */
	protected $birthday = NULL;

	/**
	 * 城乡生源
	 * 
	 * @var boolean
	 */
	protected $cxsy = FALSE;

	/**
	 * 入学前档案所在单位
	 * 
	 * @var string
	 */
	protected $rxqdaszdw = '';

	/**
	 * 档案是否转入学校
	 * 
	 * @var boolean
	 */
	protected $dasfzrxx = FALSE;

	/**
	 * 入学前户口所在地派出所
	 * 
	 * @var string
	 */
	protected $rxqhkszdpcs = '';

	/**
	 * 户口是否转入学校
	 * 
	 * @var boolean
	 */
	protected $hksfzrxx = FALSE;

	/**
	 * 手机号码
	 * 
	 * @var string
	 */
	protected $mobile = '';

	/**
	 * QQ号码
	 * 
	 * @var string
	 */
	protected $qq = '';

	/**
	 * 家庭地址
	 * 
	 * @var string
	 */
	protected $homeAddress = '';

	/**
	 * 家庭电话
	 * 
	 * @var string
	 */
	protected $homeTelephone = '';

	/**
	 * 家庭邮编
	 * 
	 * @var string
	 */
	protected $homeZip = '';

	/**
	 * 审核错误信息
	 * 
	 * @var string
	 */
	protected $errinfo = '';

	/**
	 * 是否导入就业方案
	 * 
	 * @var boolean
	 */
	protected $isimp = FALSE;

	/**
	 * 毕业年度
	 * 
	 * @var integer
	 */
	protected $lddm = 0;

	/**
	 * 学生类别
	 * 
	 * @var integer
	 */
	protected $xslx = 0;

	/**
	 * 协议书代码
	 * 
	 * @var string
	 */
	protected $xysdm = '';

	/**
	 * 协议书发放
	 * 
	 * @var boolean
	 */
	protected $xysff = FALSE;

	/**
	 * 证书编号
	 * 
	 * @var string
	 */
	protected $zsbh = '';

	/**
	 * 是否同步
	 * 
	 * @var integer
	 */
	protected $issynced = 0;

	/**
	 * 基础信息是否被确认
	 * 
	 * @var integer
	 */
	protected $issourcechecked = 0;

	/**
	 * 基础信息备注
	 * 
	 * @var string
	 */
	protected $sourceBz = '';

	/**
	 * 学校名称
	 * 
	 * @var string
	 */
	protected $schoolName = '';

	/**
	 * 院校名称
	 * 
	 * @var string
	 */
	protected $collegeName = '';

	/**
	 * 专业名称
	 * 
	 * @var string
	 */
	protected $majorName = '';

	/**
	 * 内省毕业生
	 * 
	 * @var boolean
	 */
	protected $localGraduate = FALSE;

	/**
	 * 性别
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\Gender
	 */
	protected $gender = NULL;

	/**
	 * 民族
	 * 
	 * @var \TaoJiang\SchoolJobs\Domain\Model\National
	 */
	protected $national = NULL;

	/**
	 * 政治面貌
	 * 
	 * @var \TaoJiang\SchoolJobs\Domain\Model\Political
	 */
	protected $political = NULL;

	/**
	 * 院校
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\School
	 */
	protected $school = NULL;

	/**
	 * 学历
	 * 
	 * @var \TaoJiang\SchoolJobs\Domain\Model\Diploma
	 */
	protected $diploma = NULL;

	/**
	 * 专业
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\Major
	 */
	protected $major = NULL;

	/**
	 * 培养方式
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\Pyfs
	 */
	protected $pyfsdm = NULL;

	/**
	 * 生源所在地
	 * 
	 * @var \TaoJiang\SchoolJobs\Domain\Model\Area
	 */
	protected $nativetowns = NULL;

	/**
	 * 学制
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\Schoolsystem
	 */
	protected $schoolsystem = NULL;

	/**
	 * 师范生类别
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\Sfslb
	 */
	protected $sfslbdm = NULL;

	/**
	 * 困难生类别
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\Knslb
	 */
	protected $knslbdm = NULL;

	/**
	 * 授课语言
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\Sfmyjx
	 */
	protected $sfmyjx = NULL;

	/**
	 * 所在院系
	 * 
	 * @var \TaoJiang\SchoolAgreement\Domain\Model\College
	 */
	protected $college = NULL;

	/**
	 * Returns the ksh
	 * 
	 * @return string $ksh
	 */
	public function getKsh() {
		return $this->ksh;
	}

	/**
	 * Sets the ksh
	 * 
	 * @param string $ksh
	 * @return void
	 */
	public function setKsh($ksh) {
		$this->ksh = $ksh;
	}

	/**
	 * Returns the idCard
	 * 
	 * @return string $idCard
	 */
	public function getIdCard() {
		return $this->idCard;
	}

	/**
	 * Sets the idCard
	 * 
	 * @param string $idCard
	 * @return void
	 */
	public function setIdCard($idCard) {
		$this->idCard = $idCard;
	}

	/**
	 * Returns the yxszsdm
	 * 
	 * @return string $yxszsdm
	 */
	public function getYxszsdm() {
		return $this->yxszsdm;
	}

	/**
	 * Sets the yxszsdm
	 * 
	 * @param string $yxszsdm
	 * @return void
	 */
	public function setYxszsdm($yxszsdm) {
		$this->yxszsdm = $yxszsdm;
	}

	/**
	 * Returns the zyfx
	 * 
	 * @return string $zyfx
	 */
	public function getZyfx() {
		return $this->zyfx;
	}

	/**
	 * Sets the zyfx
	 * 
	 * @param string $zyfx
	 * @return void
	 */
	public function setZyfx($zyfx) {
		$this->zyfx = $zyfx;
	}

	/**
	 * Returns the rxsj
	 * 
	 * @return \DateTime $rxsj
	 */
	public function getRxsj() {
		return $this->rxsj;
	}

	/**
	 * Sets the rxsj
	 * 
	 * @param \DateTime $rxsj
	 * @return void
	 */
	public function setRxsj(\DateTime $rxsj) {
		$this->rxsj = $rxsj;
	}

	/**
	 * Returns the bysj
	 * 
	 * @return \DateTime $bysj
	 */
	public function getBysj() {
		return $this->bysj;
	}

	/**
	 * Sets the bysj
	 * 
	 * @param \DateTime $bysj
	 * @return void
	 */
	public function setBysj(\DateTime $bysj) {
		$this->bysj = $bysj;
	}

	/**
	 * Returns the dxhwpdw
	 * 
	 * @return string $dxhwpdw
	 */
	public function getDxhwpdw() {
		return $this->dxhwpdw;
	}

	/**
	 * Sets the dxhwpdw
	 * 
	 * @param string $dxhwpdw
	 * @return void
	 */
	public function setDxhwpdw($dxhwpdw) {
		$this->dxhwpdw = $dxhwpdw;
	}

	/**
	 * Returns the campus
	 * 
	 * @return string $campus
	 */
	public function getCampus() {
		return $this->campus;
	}

	/**
	 * Sets the campus
	 * 
	 * @param string $campus
	 * @return void
	 */
	public function setCampus($campus) {
		$this->campus = $campus;
	}

	/**
	 * Returns the class
	 * 
	 * @return string $class
	 */
	public function getClass() {
		return $this->class;
	}

	/**
	 * Sets the class
	 * 
	 * @param string $class
	 * @return void
	 */
	public function setClass($class) {
		$this->class = $class;
	}

	/**
	 * Returns the number
	 * 
	 * @return string $number
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 * Sets the number
	 * 
	 * @param string $number
	 * @return void
	 */
	public function setNumber($number) {
		$this->number = $number;
	}

	/**
	 * Returns the birthday
	 * 
	 * @return \DateTime $birthday
	 */
	public function getBirthday() {
		return $this->birthday;
	}

	/**
	 * Sets the birthday
	 * 
	 * @param \DateTime $birthday
	 * @return void
	 */
	public function setBirthday(\DateTime $birthday) {
		$this->birthday = $birthday;
	}

	/**
	 * Returns the cxsy
	 * 
	 * @return boolean $cxsy
	 */
	public function getCxsy() {
		return $this->cxsy;
	}

	/**
	 * Sets the cxsy
	 * 
	 * @param boolean $cxsy
	 * @return void
	 */
	public function setCxsy($cxsy) {
		$this->cxsy = $cxsy;
	}

	/**
	 * Returns the boolean state of cxsy
	 * 
	 * @return boolean
	 */
	public function isCxsy() {
		return $this->cxsy;
	}

	/**
	 * Returns the rxqdaszdw
	 * 
	 * @return string $rxqdaszdw
	 */
	public function getRxqdaszdw() {
		return $this->rxqdaszdw;
	}

	/**
	 * Sets the rxqdaszdw
	 * 
	 * @param string $rxqdaszdw
	 * @return void
	 */
	public function setRxqdaszdw($rxqdaszdw) {
		$this->rxqdaszdw = $rxqdaszdw;
	}

	/**
	 * Returns the dasfzrxx
	 * 
	 * @return boolean $dasfzrxx
	 */
	public function getDasfzrxx() {
		return $this->dasfzrxx;
	}

	/**
	 * Sets the dasfzrxx
	 * 
	 * @param boolean $dasfzrxx
	 * @return void
	 */
	public function setDasfzrxx($dasfzrxx) {
		$this->dasfzrxx = $dasfzrxx;
	}

	/**
	 * Returns the boolean state of dasfzrxx
	 * 
	 * @return boolean
	 */
	public function isDasfzrxx() {
		return $this->dasfzrxx;
	}

	/**
	 * Returns the rxqhkszdpcs
	 * 
	 * @return string $rxqhkszdpcs
	 */
	public function getRxqhkszdpcs() {
		return $this->rxqhkszdpcs;
	}

	/**
	 * Sets the rxqhkszdpcs
	 * 
	 * @param string $rxqhkszdpcs
	 * @return void
	 */
	public function setRxqhkszdpcs($rxqhkszdpcs) {
		$this->rxqhkszdpcs = $rxqhkszdpcs;
	}

	/**
	 * Returns the hksfzrxx
	 * 
	 * @return boolean $hksfzrxx
	 */
	public function getHksfzrxx() {
		return $this->hksfzrxx;
	}

	/**
	 * Sets the hksfzrxx
	 * 
	 * @param boolean $hksfzrxx
	 * @return void
	 */
	public function setHksfzrxx($hksfzrxx) {
		$this->hksfzrxx = $hksfzrxx;
	}

	/**
	 * Returns the boolean state of hksfzrxx
	 * 
	 * @return boolean
	 */
	public function isHksfzrxx() {
		return $this->hksfzrxx;
	}

	/**
	 * Returns the mobile
	 * 
	 * @return string $mobile
	 */
	public function getMobile() {
		return $this->mobile;
	}

	/**
	 * Sets the mobile
	 * 
	 * @param string $mobile
	 * @return void
	 */
	public function setMobile($mobile) {
		$this->mobile = $mobile;
	}

	/**
	 * Returns the qq
	 * 
	 * @return string $qq
	 */
	public function getQq() {
		return $this->qq;
	}

	/**
	 * Sets the qq
	 * 
	 * @param string $qq
	 * @return void
	 */
	public function setQq($qq = '') {
		$this->qq = $qq;
	}

	/**
	 * Returns the homeAddress
	 * 
	 * @return string $homeAddress
	 */
	public function getHomeAddress() {
		return $this->homeAddress;
	}

	/**
	 * Sets the homeAddress
	 * 
	 * @param string $homeAddress
	 * @return void
	 */
	public function setHomeAddress($homeAddress) {
		$this->homeAddress = $homeAddress;
	}

	/**
	 * Returns the homeTelephone
	 * 
	 * @return string $homeTelephone
	 */
	public function getHomeTelephone() {
		return $this->homeTelephone;
	}

	/**
	 * Sets the homeTelephone
	 * 
	 * @param string $homeTelephone
	 * @return void
	 */
	public function setHomeTelephone($homeTelephone = '') {
		$this->homeTelephone = $homeTelephone;
	}

	/**
	 * Returns the homeZip
	 * 
	 * @return string $homeZip
	 */
	public function getHomeZip() {
		return $this->homeZip;
	}

	/**
	 * Sets the homeZip
	 * 
	 * @param string $homeZip
	 * @return void
	 */
	public function setHomeZip($homeZip) {
		$this->homeZip = $homeZip;
	}

	/**
	 * Returns the errinfo
	 * 
	 * @return string $errinfo
	 */
	public function getErrinfo() {
		return $this->errinfo;
	}

	/**
	 * Sets the errinfo
	 * 
	 * @param string $errinfo
	 * @return void
	 */
	public function setErrinfo($errinfo) {
		$this->errinfo = $errinfo;
	}

	/**
	 * Returns the isimp
	 * 
	 * @return boolean $isimp
	 */
	public function getIsimp() {
		return $this->isimp;
	}

	/**
	 * Sets the isimp
	 * 
	 * @param boolean $isimp
	 * @return void
	 */
	public function setIsimp($isimp) {
		$this->isimp = $isimp;
	}

	/**
	 * Returns the boolean state of isimp
	 * 
	 * @return boolean
	 */
	public function isIsimp() {
		return $this->isimp;
	}

	/**
	 * Returns the lddm
	 * 
	 * @return integer $lddm
	 */
	public function getLddm() {
		return $this->lddm;
	}

	/**
	 * Sets the lddm
	 * 
	 * @param integer $lddm
	 * @return void
	 */
	public function setLddm($lddm) {
		$this->lddm = $lddm;
	}

	/**
	 * Returns the xslx
	 * 
	 * @return integer $xslx
	 */
	public function getXslx() {
		return $this->xslx;
	}

	/**
	 * Sets the xslx
	 * 
	 * @param integer $xslx
	 * @return void
	 */
	public function setXslx($xslx) {
		$this->xslx = $xslx;
	}

	/**
	 * Returns the xysdm
	 * 
	 * @return string $xysdm
	 */
	public function getXysdm() {
		return $this->xysdm;
	}

	/**
	 * Sets the xysdm
	 * 
	 * @param string $xysdm
	 * @return void
	 */
	public function setXysdm($xysdm) {
		$this->xysdm = $xysdm;
	}

	/**
	 * Returns the xysff
	 * 
	 * @return boolean $xysff
	 */
	public function getXysff() {
		return $this->xysff;
	}

	/**
	 * Sets the xysff
	 * 
	 * @param boolean $xysff
	 * @return void
	 */
	public function setXysff($xysff) {
		$this->xysff = $xysff;
	}

	/**
	 * Returns the boolean state of xysff
	 * 
	 * @return boolean
	 */
	public function isXysff() {
		return $this->xysff;
	}

	/**
	 * Returns the zsbh
	 * 
	 * @return string $zsbh
	 */
	public function getZsbh() {
		return $this->zsbh;
	}

	/**
	 * Sets the zsbh
	 * 
	 * @param string $zsbh
	 * @return void
	 */
	public function setZsbh($zsbh) {
		$this->zsbh = $zsbh;
	}

	/**
	 * Returns the issynced
	 * 
	 * @return integer $issynced
	 */
	public function getIssynced() {
		return $this->issynced;
	}

	/**
	 * Sets the issynced
	 * 
	 * @param integer $issynced
	 * @return void
	 */
	public function setIssynced($issynced) {
		$this->issynced = $issynced;
	}

	/**
	 * Returns the issourcechecked
	 * 
	 * @return integer $issourcechecked
	 */
	public function getIssourcechecked() {
		return $this->issourcechecked;
	}

	/**
	 * Sets the issourcechecked
	 * 
	 * @param integer $issourcechecked
	 * @return void
	 */
	public function setIssourcechecked($issourcechecked) {
		$this->issourcechecked = $issourcechecked;
	}

	/**
	 * Returns the sourceBz
	 * 
	 * @return string $sourceBz
	 */
	public function getSourceBz() {
		return $this->sourceBz;
	}

	/**
	 * Sets the sourceBz
	 * 
	 * @param string $sourceBz
	 * @return void
	 */
	public function setSourceBz($sourceBz) {
		$this->sourceBz = $sourceBz;
	}

	/**
	 * Returns the schoolName
	 * 
	 * @return string $schoolName
	 */
	public function getSchoolName() {
		return $this->schoolName;
	}

	/**
	 * Sets the schoolName
	 * 
	 * @param string $schoolName
	 * @return void
	 */
	public function setSchoolName($schoolName) {
		$this->schoolName = $schoolName;
	}

	/**
	 * Returns the collegeName
	 * 
	 * @return string $collegeName
	 */
	public function getCollegeName() {
		return $this->collegeName;
	}

	/**
	 * Sets the collegeName
	 * 
	 * @param string $collegeName
	 * @return void
	 */
	public function setCollegeName($collegeName) {
		$this->collegeName = $collegeName;
	}

	/**
	 * Returns the majorName
	 * 
	 * @return string $majorName
	 */
	public function getMajorName() {
		return $this->majorName;
	}

	/**
	 * Sets the majorName
	 * 
	 * @param string $majorName
	 * @return void
	 */
	public function setMajorName($majorName) {
		$this->majorName = $majorName;
	}

	/**
	 * Returns the localGraduate
	 * 
	 * @return boolean $localGraduate
	 */
	public function getLocalGraduate() {
		return $this->localGraduate;
	}

	/**
	 * Sets the localGraduate
	 * 
	 * @param boolean $localGraduate
	 * @return void
	 */
	public function setLocalGraduate($localGraduate) {
		$this->localGraduate = $localGraduate;
	}

	/**
	 * Returns the boolean state of localGraduate
	 * 
	 * @return boolean
	 */
	public function isLocalGraduate() {
		return $this->localGraduate;
	}

	/**
	 * Returns the gender
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\Gender $gender
	 */
	public function getGender() {
		return $this->gender;
	}

	/**
	 * Sets the gender
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Gender $gender
	 * @return void
	 */
	public function setGender(\TaoJiang\SchoolAgreement\Domain\Model\Gender $gender = null) {
		$this->gender = $gender;
	}

	/**
	 * Returns the national
	 * 
	 * @return \TaoJiang\SchoolJobs\Domain\Model\National $national
	 */
	public function getNational() {
		return $this->national;
	}

	/**
	 * Sets the national
	 * 
	 * @param \TaoJiang\SchoolJobs\Domain\Model\National $national
	 * @return void
	 */
	public function setNational(\TaoJiang\SchoolJobs\Domain\Model\National $national = null) {
		$this->national = $national;
	}

	/**
	 * Returns the political
	 * 
	 * @return \TaoJiang\SchoolJobs\Domain\Model\Political $political
	 */
	public function getPolitical() {
		return $this->political;
	}

	/**
	 * Sets the political
	 * 
	 * @param \TaoJiang\SchoolJobs\Domain\Model\Political $political
	 * @return void
	 */
	public function setPolitical(\TaoJiang\SchoolJobs\Domain\Model\Political $political = null) {
		$this->political = $political;
	}

	/**
	 * Returns the school
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\School $school
	 */
	public function getSchool() {
		return $this->school;
	}

	/**
	 * Sets the school
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\School $school
	 * @return void
	 */
	public function setSchool(\TaoJiang\SchoolAgreement\Domain\Model\School $school = null) {
		$this->school = $school;
	}

	/**
	 * Returns the diploma
	 * 
	 * @return \TaoJiang\SchoolJobs\Domain\Model\Diploma $diploma
	 */
	public function getDiploma() {
		return $this->diploma;
	}

	/**
	 * Sets the diploma
	 * 
	 * @param \TaoJiang\SchoolJobs\Domain\Model\Diploma $diploma
	 * @return void
	 */
	public function setDiploma(\TaoJiang\SchoolJobs\Domain\Model\Diploma $diploma = null) {
		$this->diploma = $diploma;
	}

	/**
	 * Returns the major
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\Major $major
	 */
	public function getMajor() {
		return $this->major;
	}

	/**
	 * Sets the major
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Major $major
	 * @return void
	 */
	public function setMajor(\TaoJiang\SchoolAgreement\Domain\Model\Major $major = null) {
		$this->major = $major;
	}

	/**
	 * Returns the pyfsdm
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\Pyfs $pyfsdm
	 */
	public function getPyfsdm() {
		return $this->pyfsdm;
	}

	/**
	 * Sets the pyfsdm
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Pyfs $pyfsdm
	 * @return void
	 */
	public function setPyfsdm(\TaoJiang\SchoolAgreement\Domain\Model\Pyfs $pyfsdm = null) {
		$this->pyfsdm = $pyfsdm;
	}

	/**
	 * Returns the nativetowns
	 * 
	 * @return \TaoJiang\SchoolJobs\Domain\Model\Area $nativetowns
	 */
	public function getNativetowns() {
		return $this->nativetowns;
	}

	/**
	 * Sets the nativetowns
	 * 
	 * @param \TaoJiang\SchoolJobs\Domain\Model\Area $nativetowns
	 * @return void
	 */
	public function setNativetowns(\TaoJiang\SchoolJobs\Domain\Model\Area $nativetowns = null) {
		$this->nativetowns = $nativetowns;
	}

	/**
	 * Returns the schoolsystem
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\Schoolsystem $schoolsystem
	 */
	public function getSchoolsystem() {
		return $this->schoolsystem;
	}

	/**
	 * Sets the schoolsystem
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Schoolsystem $schoolsystem
	 * @return void
	 */
	public function setSchoolsystem(\TaoJiang\SchoolAgreement\Domain\Model\Schoolsystem $schoolsystem = null) {
		$this->schoolsystem = $schoolsystem;
	}

	/**
	 * Returns the sfslbdm
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\Sfslb $sfslbdm
	 */
	public function getSfslbdm() {
		return $this->sfslbdm;
	}

	/**
	 * Sets the sfslbdm
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Sfslb $sfslbdm
	 * @return void
	 */
	public function setSfslbdm(\TaoJiang\SchoolAgreement\Domain\Model\Sfslb $sfslbdm = null) {
		$this->sfslbdm = $sfslbdm;
	}

	/**
	 * Returns the knslbdm
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\Knslb $knslbdm
	 */
	public function getKnslbdm() {
		return $this->knslbdm;
	}

	/**
	 * Sets the knslbdm
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Knslb $knslbdm
	 * @return void
	 */
	public function setKnslbdm(\TaoJiang\SchoolAgreement\Domain\Model\Knslb $knslbdm = null) {
		$this->knslbdm = $knslbdm;
	}

	/**
	 * Returns the sfmyjx
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\Sfmyjx $sfmyjx
	 */
	public function getSfmyjx() {
		return $this->sfmyjx;
	}

	/**
	 * Sets the sfmyjx
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\Sfmyjx $sfmyjx
	 * @return void
	 */
	public function setSfmyjx(\TaoJiang\SchoolAgreement\Domain\Model\Sfmyjx $sfmyjx = null) {
		$this->sfmyjx = $sfmyjx;
	}

	/**
	 * Returns the college
	 * 
	 * @return \TaoJiang\SchoolAgreement\Domain\Model\College $college
	 */
	public function getCollege() {
		return $this->college;
	}

	/**
	 * Sets the college
	 * 
	 * @param \TaoJiang\SchoolAgreement\Domain\Model\College $college
	 * @return void
	 */
	public function setCollege(\TaoJiang\SchoolAgreement\Domain\Model\College $college = null) {
		$this->college = $college;
	}

}