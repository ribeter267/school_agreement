<?php
namespace TaoJiang\SchoolAgreement\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 专业信息数据表
 */
class Major extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * 学校代码
	 * 
	 * @var string
	 */
	protected $xxdm = '';

	/**
	 * 学校名称
	 * 
	 * @var string
	 */
	protected $xxmc = '';

	/**
	 * 学生类型
	 * 
	 * @var integer
	 */
	protected $xslx = 0;

	/**
	 * 专业号
	 * 
	 * @var integer
	 */
	protected $zyh = 0;

	/**
	 * 院系所号
	 * 
	 * @var string
	 */
	protected $yzsh = '';

	/**
	 * 院系所名称
	 * 
	 * @var string
	 */
	protected $yxsmc = '';

	/**
	 * 专业名称
	 * 
	 * @var string
	 */
	protected $zymc = '';

	/**
	 * 专业简称
	 * 
	 * @var string
	 */
	protected $zyjc = '';

	/**
	 * Returns the xxdm
	 * 
	 * @return string $xxdm
	 */
	public function getXxdm() {
		return $this->xxdm;
	}

	/**
	 * Sets the xxdm
	 * 
	 * @param string $xxdm
	 * @return void
	 */
	public function setXxdm($xxdm) {
		$this->xxdm = $xxdm;
	}

	/**
	 * Returns the xxmc
	 * 
	 * @return string $xxmc
	 */
	public function getXxmc() {
		return $this->xxmc;
	}

	/**
	 * Sets the xxmc
	 * 
	 * @param string $xxmc
	 * @return void
	 */
	public function setXxmc($xxmc) {
		$this->xxmc = $xxmc;
	}

	/**
	 * Returns the xslx
	 * 
	 * @return integer $xslx
	 */
	public function getXslx() {
		return $this->xslx;
	}

	/**
	 * Sets the xslx
	 * 
	 * @param integer $xslx
	 * @return void
	 */
	public function setXslx($xslx) {
		$this->xslx = $xslx;
	}

	/**
	 * Returns the zyh
	 * 
	 * @return integer $zyh
	 */
	public function getZyh() {
		return $this->zyh;
	}

	/**
	 * Sets the zyh
	 * 
	 * @param integer $zyh
	 * @return void
	 */
	public function setZyh($zyh) {
		$this->zyh = $zyh;
	}

	/**
	 * Returns the yzsh
	 * 
	 * @return string $yzsh
	 */
	public function getYzsh() {
		return $this->yzsh;
	}

	/**
	 * Sets the yzsh
	 * 
	 * @param string $yzsh
	 * @return void
	 */
	public function setYzsh($yzsh) {
		$this->yzsh = $yzsh;
	}

	/**
	 * Returns the yxsmc
	 * 
	 * @return string $yxsmc
	 */
	public function getYxsmc() {
		return $this->yxsmc;
	}

	/**
	 * Sets the yxsmc
	 * 
	 * @param string $yxsmc
	 * @return void
	 */
	public function setYxsmc($yxsmc) {
		$this->yxsmc = $yxsmc;
	}

	/**
	 * Returns the zymc
	 * 
	 * @return string $zymc
	 */
	public function getZymc() {
		return $this->zymc;
	}

	/**
	 * Sets the zymc
	 * 
	 * @param string $zymc
	 * @return void
	 */
	public function setZymc($zymc) {
		$this->zymc = $zymc;
	}

	/**
	 * Returns the zyjc
	 * 
	 * @return string $zyjc
	 */
	public function getZyjc() {
		return $this->zyjc;
	}

	/**
	 * Sets the zyjc
	 * 
	 * @param string $zyjc
	 * @return void
	 */
	public function setZyjc($zyjc) {
		$this->zyjc = $zyjc;
	}

}