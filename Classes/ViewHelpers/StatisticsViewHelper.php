<?php
namespace TaoJiang\SchoolAgreement\ViewHelpers;
class StatisticsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Go through all given classes which implement the mediainterface
	 * and use the proper ones to render the media element
	 *
	 * @param string $classes list of classes which are used to render the media object
	 * @param Tx_News_Domain_Model_FileReference $element Current media object
	 * @param integer $width width
	 * @param integer $height height
	 * @return string
	 * @throws UnexpectedValueException
	 */
	public function render($datas) {
	
		$registed = $datas->count(); $sourcechecked = 0; $jobchecked = 0;
		
		foreach($datas as $data){
			if($data->getIssourcechecked()) $sourcechecked++;
			if($data->getIsjobchecked()) $jobchecked++;
		}
		
		$content = '
			<div class="form-group">
				已注册：'.$registed.' 人， 基础信息确认：'.$sourcechecked.' 人， 就业信息确认：'.$jobchecked.'人。
			</div>
		';
		
		return $content;
	}

}
