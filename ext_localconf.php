<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TaoJiang.' . $_EXTKEY,
	'Manager',
	array(
		'Student' => 'statistics, list, show, multiprint, autoprint',
		'Teacher' => 'list, show, edit, update, delete, active, multiactive, multidelete'
	),
	// non-cacheable actions
	array(
		'Student' => 'autoprint',
		'Teacher' => 'update, delete, active, multiactive, multidelete'
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TaoJiang.' . $_EXTKEY,
	'Student',
	array(
		'Student' => 'edit, update, print, bind, bindup',
		'Job' => 'show, edit, update',
		
	),
	// non-cacheable actions
	array(
		'Student' => 'update, bindup',
		'Job' => 'update',
		
	)
);



$TYPO3_CONF_VARS['SC_OPTIONS']['scheduler']['tasks']['Tx_SchoolAgreement_Task_DictionaryImportTask'] = array(
	'extension' => $_EXTKEY,
	'title' => '字典数据导入',
	'description' => '',
);

$TYPO3_CONF_VARS['SC_OPTIONS']['scheduler']['tasks']['Tx_SchoolAgreement_Task_SchoolImportTask'] = array(
	'extension' => $_EXTKEY,
	'title' => '院校及关系数据导入',
	'description' => '',
);

$TYPO3_CONF_VARS['SC_OPTIONS']['scheduler']['tasks']['Tx_SchoolAgreement_Task_StudentRefreshTask'] = array(
	'extension' => $_EXTKEY,
	'title' => '已验绑定学生信息更新',
	'description' => '',
);

