<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Manager',
	'就业协议书::管理'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Student',
	'就业协议书::学生'
);


$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY);
$pluginManager = strtolower($extensionName) . '_manager';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginManager] = 'layout,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginManager] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginManager, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_manager.xml');

$pluginStudent = strtolower($extensionName) . '_student';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginStudent] = 'layout,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginStudent] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginStudent, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_student.xml');



\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', '就业协议书');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_gender', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_gender.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_gender');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_gender'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_gender',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Gender.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_gender.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_pyfs', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_pyfs.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_pyfs');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_pyfs'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_pyfs',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Pyfs.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_pyfs.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_schoolsystem', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_schoolsystem.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_schoolsystem');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_schoolsystem'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_schoolsystem',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Schoolsystem.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_schoolsystem.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_sfslb', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_sfslb.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_sfslb');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_sfslb'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_sfslb',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Sfslb.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_sfslb.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_knslb', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_knslb.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_knslb');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_knslb'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_knslb',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Knslb.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_knslb.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_byqx', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_byqx.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_byqx');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_byqx'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_byqx',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Byqx.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_byqx.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_bdzqflb', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_bdzqflb.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_bdzqflb');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_bdzqflb'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_bdzqflb',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Bdzqflb.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_bdzqflb.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_sfmyjx', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_sfmyjx.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_sfmyjx');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_sfmyjx'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_sfmyjx',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Sfmyjx.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_sfmyjx.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_school', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_school.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_school');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_school'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_school',
		'label' => 'xxmc',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'xxmc,xxywmc,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/School.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_school.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_college', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_college.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_college');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_college'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_college',
		'label' => 'xslx',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'xslx,yzsh,yxsmc,yxsywmc,yxsjc,xxdm,xxmc,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/College.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_college.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_schoolagreement_domain_model_major', 'EXT:school_agreement/Resources/Private/Language/locallang_csh_tx_schoolagreement_domain_model_major.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_schoolagreement_domain_model_major');
$GLOBALS['TCA']['tx_schoolagreement_domain_model_major'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_major',
		'label' => 'zymc',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'rootLevel' => 1,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'xxdm,xxmc,xslx,zyh,yzsh,yxsmc,zymc,zyjc,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Major.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_schoolagreement_domain_model_major.gif'
	),
);

if (!isset($GLOBALS['TCA']['fe_users']['ctrl']['type'])) {
	if (file_exists($GLOBALS['TCA']['fe_users']['ctrl']['dynamicConfigFile'])) {
		require_once($GLOBALS['TCA']['fe_users']['ctrl']['dynamicConfigFile']);
	}
	// no type field defined, so we define it here. This will only happen the first time the extension is installed!!
	$GLOBALS['TCA']['fe_users']['ctrl']['type'] = 'tx_extbase_type';
	$tempColumns = array();
	$tempColumns[$GLOBALS['TCA']['fe_users']['ctrl']['type']] = array(
		'exclude' => 1,
		'label'   => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement.tx_extbase_type',
		'config' => array(
			'type' => 'select',
			'items' => array(),
			'size' => 1,
			'maxitems' => 1,
		)
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumns, 1);
}

$tmp_school_agreement_columns = array(

	'dwmc' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dwmc',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'dwzzjgdm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dwzzjgdm',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'dwlxr' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dwlxr',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'lxrdh' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.lxrdh',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'lxrsj' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.lxrsj',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'lxrdzyx' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.lxrdzyx',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'lxrcz' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.lxrcz',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'dwdz' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dwdz',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'dwyb' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dwyb',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'bdzqwdwmc' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.bdzqwdwmc',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'dazjdwmc' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dazjdwmc',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'dazjdwdz' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dazjdwdz',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'dazjdwyb' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dazjdwyb',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'hkqzdz' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.hkqzdz',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'bdzbh' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.bdzbh',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'bdqssj' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.bdqssj',
		'config' => array(
			'type' => 'input',
			'size' => 7,
			'eval' => 'date',
			'checkbox' => 1,
			'default' => time()
		),
	),
	'job_bz' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.job_bz',
		'config' => array(
			'type' => 'text',
			'cols' => 40,
			'rows' => 15,
			'eval' => 'trim'
		)
	),
	'isjobchecked' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.isjobchecked',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'byqxdm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.byqxdm',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_byqx',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'dwszddm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dwszddm',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schooljobs_domain_model_area',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'gzzwlbdm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.gzzwlbdm',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schooljobs_domain_model_jobcategory',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'bdzqflbdm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.bdzqflbdm',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_bdzqflb',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'qwdwszddm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.qwdwszddm',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schooljobs_domain_model_area',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'dwxzdm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dwxzdm',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schooljobs_domain_model_companyproperty',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'dwhydm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job.dwhydm',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schooljobs_domain_model_industry',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users',$tmp_school_agreement_columns);

$GLOBALS['TCA']['fe_users']['types']['Tx_SchoolAgreement_Job']['showitem'] = $TCA['fe_users']['types']['0']['showitem'];
$GLOBALS['TCA']['fe_users']['types']['Tx_SchoolAgreement_Job']['showitem'] .= ',--div--;LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_job,';
$GLOBALS['TCA']['fe_users']['types']['Tx_SchoolAgreement_Job']['showitem'] .= 'dwmc, dwzzjgdm, dwlxr, lxrdh, lxrsj, lxrdzyx, lxrcz, dwdz, dwyb, bdzqwdwmc, dazjdwmc, dazjdwdz, dazjdwyb, hkqzdz, bdzbh, bdqssj, job_bz, isjobchecked, byqxdm, dwszddm, gzzwlbdm, bdzqflbdm, qwdwszddm, dwxzdm, dwhydm';

$GLOBALS['TCA']['fe_users']['columns'][$TCA['fe_users']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:fe_users.tx_extbase_type.Tx_SchoolAgreement_Job','Tx_SchoolAgreement_Job');

$tmp_school_agreement_columns = array(

	'school' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_teacher.school',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_school',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	
	'mobile' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.mobile',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users',$tmp_school_agreement_columns);

$GLOBALS['TCA']['fe_users']['types']['Tx_SchoolAgreement_Teacher']['showitem'] = $TCA['fe_users']['types']['0']['showitem'];
$GLOBALS['TCA']['fe_users']['types']['Tx_SchoolAgreement_Teacher']['showitem'] .= ',--div--;LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_teacher,';
$GLOBALS['TCA']['fe_users']['types']['Tx_SchoolAgreement_Teacher']['showitem'] .= 'school, mobile';

$GLOBALS['TCA']['fe_users']['columns'][$TCA['fe_users']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:fe_users.tx_extbase_type.Tx_SchoolAgreement_Teacher','Tx_SchoolAgreement_Teacher');

$tmp_school_agreement_columns = array(

	'ksh' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.ksh',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'id_card' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.id_card',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'yxszsdm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.yxszsdm',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'zyfx' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.zyfx',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'rxsj' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.rxsj',
		'config' => array(
			'type' => 'input',
			'size' => 7,
			'eval' => 'date',
			'checkbox' => 1,
			'default' => time()
		),
	),
	'bysj' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.bysj',
		'config' => array(
			'type' => 'input',
			'size' => 7,
			'eval' => 'date',
			'checkbox' => 1,
			'default' => time()
		),
	),
	'dxhwpdw' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.dxhwpdw',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'campus' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.campus',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'class' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.class',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'number' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.number',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'birthday' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.birthday',
		'config' => array(
			'type' => 'input',
			'size' => 7,
			'eval' => 'date',
			'checkbox' => 1,
			'default' => time()
		),
	),
	'cxsy' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.cxsy',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'rxqdaszdw' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.rxqdaszdw',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'dasfzrxx' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.dasfzrxx',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'rxqhkszdpcs' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.rxqhkszdpcs',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'hksfzrxx' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.hksfzrxx',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'mobile' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.mobile',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'qq' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.qq',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'home_address' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.home_address',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'home_telephone' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.home_telephone',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'home_zip' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.home_zip',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'errinfo' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.errinfo',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'isimp' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.isimp',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'lddm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.lddm',
		'config' => array(
			'type' => 'input',
			'size' => 4,
			'eval' => 'int'
		)
	),
	'xslx' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.xslx',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array('LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.xslx.I.0', 0),
				array('LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.xslx.I.1', 1),
			),
			'size' => 1,
			'maxitems' => 1,
			'eval' => ''
		),
	),
	'xysdm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.xysdm',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'xysff' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.xysff',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'zsbh' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.zsbh',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'issynced' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.issynced',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array('LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.issynced.I.0', 0),
				array('LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.issynced.I.1', 1),
				array('LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.issynced.I.2', 2),
			),
			'size' => 1,
			'maxitems' => 1,
			'eval' => ''
		),
	),
	'issourcechecked' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.issourcechecked',
		'config' => array(
			'type' => 'check',
			'default' => 0
		),
	),
	'source_bz' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.source_bz',
		'config' => array(
			'type' => 'text',
			'cols' => 40,
			'rows' => 15,
			'eval' => 'trim'
		)
	),
	'school_name' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.school_name',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'college_name' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.college_name',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'major_name' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.major_name',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'local_graduate' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.local_graduate',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'gender' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.gender',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_gender',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'national' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.national',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schooljobs_domain_model_national',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'political' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.political',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schooljobs_domain_model_political',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'school' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.school',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_school',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'diploma' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.diploma',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schooljobs_domain_model_diploma',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'major' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.major',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_major',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'pyfsdm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.pyfsdm',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_pyfs',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'nativetowns' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.nativetowns',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schooljobs_domain_model_area',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'schoolsystem' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.schoolsystem',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_schoolsystem',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'sfslbdm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.sfslbdm',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_sfslb',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'knslbdm' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.knslbdm',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_knslb',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'sfmyjx' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.sfmyjx',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_sfmyjx',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'college' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student.college',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_schoolagreement_domain_model_college',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users',$tmp_school_agreement_columns);

$GLOBALS['TCA']['fe_users']['types']['Tx_SchoolAgreement_Student']['showitem'] = $TCA['fe_users']['types']['0']['showitem'];
$GLOBALS['TCA']['fe_users']['types']['Tx_SchoolAgreement_Student']['showitem'] .= ',--div--;LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:tx_schoolagreement_domain_model_student,';
$GLOBALS['TCA']['fe_users']['types']['Tx_SchoolAgreement_Student']['showitem'] .= 'ksh, id_card, yxszsdm, zyfx, rxsj, bysj, dxhwpdw, campus, class, number, birthday, cxsy, rxqdaszdw, dasfzrxx, rxqhkszdpcs, hksfzrxx, mobile, qq, home_address, home_telephone, home_zip, errinfo, isimp, lddm, xslx, xysdm, xysff, zsbh, issynced, issourcechecked, source_bz, school_name, college_name, major_name, local_graduate, gender, national, political, school, diploma, major, pyfsdm, nativetowns, schoolsystem, sfslbdm, knslbdm, sfmyjx, college';

$GLOBALS['TCA']['fe_users']['columns'][$TCA['fe_users']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:school_agreement/Resources/Private/Language/locallang_db.xlf:fe_users.tx_extbase_type.Tx_SchoolAgreement_Student','Tx_SchoolAgreement_Student');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', $GLOBALS['TCA']['fe_users']['ctrl']['type'],'','after:' . $TCA['fe_users']['ctrl']['label']);
