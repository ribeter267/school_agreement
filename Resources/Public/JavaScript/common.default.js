;(function($){
    $(function(){
	
		$(".select2").select2();
        $(".select2mininput").select2({minimumInputLength: 2});
        $(".select2nolimit").select2({placeholder: "不限", allowClear: true});
        $(".select2nolimitmininput").select2({minimumInputLength: 2, placeholder: "不限", allowClear: true});
		$('.select2clear').bind('click',function(){
			$(this).closest('.col-sm-10').find('select').select2("val", 0);;
		});
		
		
		$('.printBtn').bind('click',function(){ //打印
			$('.print_area').printArea({mode:'popup',popClose:true,popWd:1200,popHt:800});
		});
		
		
		$('.btn-del').click(function(){
			if(confirm('确认删除')) return true;
			return false;
		});
		
		//全选&取消全选
		$('input[type="checkbox"].selectall').bind('click', function() { 
			$(this).closest('table').find('.sel').prop("checked", this.checked);
		}); 
		
		//批量处理
		$('.btn-multiaction-all').bind('click',function(){
			var items='';
			$('input[type="checkbox"].sel').each(function(){
				if($(this).prop("checked")){
					items += $(this).val()+',';
				}
			});
			
			if(items == ''){
				alert('没有选中任何项目，不能批量操作');
			}else{
				$('.multiaction-items').val(items);
				return true;
			}
			return false;
		});
		
		
		
		
	
        if ( $("#school_agreement_bind_form").length > 0 ) { 
			$("#school_agreement_bind_form").validate({
				errorElement: "span", 
				errorClass : 'help-block',
				focusInvalid : false,
				rules: {
					'tx_schoolagreement_student[student][idCard]': {
						required: true,
						isIdcard: true,
					},
					'tx_schoolagreement_student[student][name]': {
						required: true,
					},
				},
				errorPlacement: function(error, element) {  
					error.appendTo(element.parent());  
				},
				highlight : function(element) {
					$(element).closest('.form-group').addClass('has-error');
				},
				success : function(label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},
			});
		}
		
		
		if ( $("#school_agreement_teacher_eidt_form").length > 0 ) { 
			$("#school_agreement_teacher_eidt_form").validate({
				errorElement: "span", 
				errorClass : 'help-block',
				focusInvalid : false,
				rules: {
					'tx_schoolagreement_manager[teacher][name]': {
						required: true,
					},
					'tx_schoolagreement_manager[teacher][school]': {
						required: true,
					},
					'tx_schoolagreement_manager[teacher][mobile]': {
						required: true,
						isMobile: true,
					},
				},
				errorPlacement: function(error, element) {  
					error.appendTo(element.parent());  
				},
				highlight : function(element) {
					$(element).closest('.form-group').addClass('has-error');
				},
				success : function(label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},
			});
		}
		
		
		if ( $("#school_agreement_student_eidt_form").length > 0 ) { 
			$("#school_agreement_student_eidt_form").validate({
				errorElement: "span", 
				errorClass : 'help-block',
				focusInvalid : false,
				rules: {
					'tx_schoolagreement_student[student][cxsy]': {
						required: true,
					},
					'tx_schoolagreement_student[student][dasfzrxx]': {
						required: true,
					},
					'tx_schoolagreement_student[student][hksfzrxx]': {
						required: true,
					},
					'tx_schoolagreement_student[student][email]': {
						required: true,
						email: true,
					},
					'tx_schoolagreement_student[student][homeAddress]': {
						required: true,
					},
					'tx_schoolagreement_student[student][homeZip]': {
						required: true,
						isZip: true,
					},
					'tx_schoolagreement_student[student][rxqdaszdw]': {
						required: true,
					},
					'tx_schoolagreement_student[student][rxqhkszdpcs]': {
						required: true,
					},
					'tx_schoolagreement_student[student][mobile]': {
						required: true,
						isMobile: true,
					},
					'tx_schoolagreement_student[student][qq]': {
						required: true,
						isQq: true,
					},
					'tx_schoolagreement_student[student][homeTelephone]': {
						required: true,
						isPhone: true,
					},
				},
				errorPlacement: function(error, element) {  
					error.appendTo(element.parent());  
				},
				highlight : function(element) {
					$(element).closest('.form-group').addClass('has-error');
				},
				success : function(label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},
			});
		}
			
			
		if ( $("#school_agreement_job_eidt_form").length > 0 ) { 
			$("#school_agreement_job_eidt_form").validate({
				errorElement: "span", 
				errorClass : 'help-block',
				focusInvalid : false,
				rules: {
					'tx_schoolagreement_student[job][byqxdm]': {
						required: true,
					},'tx_schoolagreement_student[job][dwmc]': {
						required: true,
					},'tx_schoolagreement_student[job][dwzzjgdm]': {
						required: true,
					},'tx_schoolagreement_student[job][dwxzdm]': {
						required: true,
					},'tx_schoolagreement_student[job][dwhydm]': {
						required: true,
					},'tx_schoolagreement_student[job][dwszddm]': {
						required: true,
					},'tx_schoolagreement_student[job][gzzwlbdm]': {
						required: true,
					},'tx_schoolagreement_student[job][dwlxr]': {
						required: true,
					},'tx_schoolagreement_student[job][lxrdh]': {
						required: true,
						isPhone: true,
					},'tx_schoolagreement_student[job][lxrsj]': {
						required: true,
						isMobile: true,
					},'tx_schoolagreement_student[job][lxrdzyx]': {
						required: true,
						email: true,
					},'tx_schoolagreement_student[job][lxrcz]': {
						required: true,
						isPhone: true,
					},'tx_schoolagreement_student[job][dwdz]': {
						required: true,
					},'tx_schoolagreement_student[job][dwyb]': {
						required: true,
						isZip:true,
					},'tx_schoolagreement_student[job][bdzqflbdm]': {
						required: true,
					},'tx_schoolagreement_student[job][bdzqwdwmc]': {
						required: true,
					},'tx_schoolagreement_student[job][qwdwszddm]': {
						required: true,
					},'tx_schoolagreement_student[job][dazjdwmc]': {
						required: true,
					},'tx_schoolagreement_student[job][dazjdwdz]': {
						required: true,
					},'tx_schoolagreement_student[job][dazjdwyb]': {
						required: true,
					},'tx_schoolagreement_student[job][hkqzdz]': {
						required: true,
					},
				},
				errorPlacement: function(error, element) {  
					error.appendTo(element.parent());  
				},
				highlight : function(element) {
					$(element).closest('.form-group').addClass('has-error');
				},
				success : function(label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},
			});
		}
    });
})(jQuery);